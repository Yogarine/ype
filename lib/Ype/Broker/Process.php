<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2016 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype\Broker
 * @author    Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2016 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/** Ensure YPE_EXECUTABLE is set. */
if(false == defined('YPE_EXECUTABLE'))
{
	if(isset($argv[0]))
	{
		define('YPE_EXECUTABLE', $argv[0]);
	}
	else
	{
		throw new Exception("YPE_EXECUTABLE isn't set and we're not running in CLI");
	}
}

/**
 * @package Ype\Broker
 */
class Ype_Broker_Process extends Ype_Broker
{
	/** int */
	const READ_DATA_CHUNK_LENGTH = 1024;

	/** @var Ype_NonBlockingStream_Socket_Client */
	protected $socketReadClient;

//	/** @var Ype_NonBlockingStream_Socket_Client */
//	protected $socketWriteClient;

	/**
	 * @param $serverAddress
	 * @param $serverPort
	 */
	public function __construct($serverAddress, $serverPort = null)
	{
		parent::__construct();

		$this->socketReadClient = new Ype_NonBlockingStream_Socket_Client($this->streamHandler, $serverAddress, $serverPort);
		$this->socketReadClient->registerReadCallback(array($this, 'onSocketRead'));
		$this->socketReadClient->registerDisconnectCallback('Ype::quit()');
		$this->socketReadClient->connect();

//		$this->socketWriteClient = new Ype_NonBlockingStream_Socket_Client($this->streamHandler, $serverAddress, $serverPort);
//		$this->socketWriteClient->registerDisconnectCallback('Ype::quit()');
//		$this->socketWriteClient->connect();
	}

	/**
	 * @param Ype_AbstractPlugin $plugin
	 * @return boolean True iff registration was successful.
	 */
	public function registerPlugin(Ype_AbstractPlugin $plugin)
	{
		$registrationWasSuccessful = parent::registerPlugin($plugin);
		if($registrationWasSuccessful)
		{
			$this->sendMessage('initializePlugin', array(), $plugin->getIdentifier());
		}

		return $registrationWasSuccessful;
	}
	
	public function unregisterPlugin($pluginId)
	{
		parent::unregisterPlugin($pluginId);
	}

	/**
	 * @param string $messageName
	 * @param array $namedParams
	 * @param int|string $target
	 * @param int $messageToken
	 * @param int $senderId
	 * @param string $origin
	 * @return int Message Token.
	 */
	public function sendMessage($messageName, array $namedParams = array(), $target = null, $messageToken = null, $senderId = null,
	                              $origin = self::ORIGIN_LOCAL)
	{
		$newMessageToken = parent::sendMessage($messageName, $namedParams, $target, $messageToken, $senderId);
		if(null !== $newMessageToken)
		{
			$messageToken = $newMessageToken;
		}

		if($origin == self::ORIGIN_LOCAL)
		{
			if($messageToken === null)
			{
				$messageToken = ++$this->messageTokenIndex;
			}
			$senderId = $this->currentContextPluginId;

			$this->socketReadClient->appendOutgoingData(self::encodeMessage($messageName, $namedParams, $messageToken, $senderId));
		}

		return $messageToken;
	}

	/**
	 * @param callable $callback
	 * @param array    $params
	 * @param int      $contextPluginId
	 */
	protected function callback($callback, $params, $contextPluginId)
	{
		$oldSenderPluginId            = $this->senderPluginId;

		$this->senderPluginId         = $this->currentContextPluginId;
		$this->currentContextPluginId = $contextPluginId;
		try
		{
			call_user_func_array($callback, $params);
		}
		catch(Exception $e)
		{
			Ype::message('writeToStandardOutput', array("Exception: {$e->getMessage()} in {$e->getFile()}:{$e->getLine()}"));
		}
		$this->currentContextPluginId = $this->senderPluginId;
		$this->senderPluginId         = $oldSenderPluginId;
	}

	public function closeAll()
	{
		$this->socketReadClient->stop();
//		$this->socketWriteClient->stop();
	}

	/**
	 * @param $line
	 * @internal param resource $stream
	 */
	public function onSocketRead($line)
	{
		if($line !== '')
		{
			try
			{
				$stuff = self::decodeMessage($line);
				list($messageName, $params, $messageToken, $senderId, $targetId) = $stuff;

				Ype_Log::debug('stuff', $stuff);
				$this->currentContextPluginId = $senderId;
				self::sendMessage($messageName, $params, $targetId, $messageToken, $senderId, self::ORIGIN_UPSTREAM);
			}
			catch(Exception $e)
			{
				Ype_Log::error('Plugin', "Uncaught Exception: {$e->getMessage()} in {$e->getFile()} on line {$e->getLine()}\n");
			}
		}
	}

	/**
	 * Handles PHP errors.
	 *
	 * @param  int	    $errno
	 * @param  string   $errstr
	 * @param  string   $errfile
	 * @param  int	    $errline
	 * @param  array    $errcontext
	 * @return boolean
	 */
	public function handleError($errno, $errstr, $errfile = '', $errline = 0, array $errcontext = array())
	{
		if(strpos($errfile, 'eval()') !== false)
		{
			$errfile = "eval()'d code";
		}
		Ype_Log::error('ype', "PHP error: ({$errno}) {$errstr} in {$errfile} on line {$errline}");
		return true;
	}

	public function quit()
	{
		$this->socketReadClient->stopListening();
	}
}