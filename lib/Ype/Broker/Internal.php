<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2016 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype\Broker
 * @author    Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2016 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/** We depend on lcfirst() */
require_once 'ype_compatibility.php';

/**
 * @package Ype\Broker
 */
class Ype_Broker_Internal extends Ype_Broker
{
	/** @var Ype_NonBlockingStream_Process[] */
	protected $processes = array();

	/** @var Ype_ProcessCompatiblePluginInterface[] */
	protected $processPlugins = array();

	/** @var Ype_NonBlockingStream_Socket_Server */
	protected $socketServer;

	/** @var array Two dimensional array that keeps track of which sockets have which plugins. */
	protected $socketPlugins = array();

	/** @var string[] Array that keeps track of which plugins are in which sockets. */
	protected $pluginSockets = array();

	/** @var array[] */
	protected $streamPluginIdentifiers = array();

	/** @var string[] List of line buffers indexed by connectionId. */
	protected $lineBuffers = array();

	public function __construct()
	{
		parent::__construct();

		$this->socketServer = $this->_startSocketServer();
	}

	public function registerPlugin(Ype_AbstractPlugin $plugin)
	{
		$process = null;
		if($plugin instanceof Ype_ProcessCompatiblePluginInterface)
		{
			$process = $this->_startPluginProcess($plugin);
			if(null !== $process)
			{
				$this->processes[$plugin->getIdentifier()]       = $process;
				$this->processPlugins[$process->getIdentifier()] = $plugin;
			}
			else
			{
				Ype_Log::warning(__CLASS__, "Unable to start process for Plugin '{$plugin->getName()}' with identifier {$plugin->getIdentifier()}.");
			}
		}

		if(parent::registerPlugin($plugin))
		{
			// If this plugin is loaded in a process, let the process' own broker take care of it, otherwise signal
			// that the plugin should initialize itself.
			if(null === $process)
			{
				$this->sendMessage('initializePlugin', array(), $plugin->getIdentifier());
			}
			$this->sendMessage('pluginRegistered', array(), null, null, $plugin->getIdentifier());
		}
		elseif(null !== $process)
		{
			// Plugin registration failed. Close the process if it was created.
			Ype_Log::error(__CLASS__, "Plugin registration failed.");
			$process->close();

			unset($this->processes[$plugin->getIdentifier()]);
			unset($this->processPlugins[$process->getIdentifier()]);
			
			return false;
		}

		return true;
	}
	
	public function unregisterPlugin($pluginId)
	{
		Ype_Log::debugFunctionCall();

		if(isset($this->processes[$pluginId]))
		{
			$this->processes[$pluginId]->close();
			unset($this->processes[$pluginId]);
		}
		parent::unregisterPlugin($pluginId);
	}

	/**
	 * @param callable $callback
	 * @param array    $params
	 * @param int      $contextPluginId
	 * @throws Exception
	 */
	protected function callback($callback, $params, $contextPluginId)
	{
		$plugin = $callback[0];
		$method = $callback[1];

		if($plugin instanceof Ype_PluginInterface)
		{
			if(isset($this->processes[$plugin->getIdentifier()]))
			{
				Ype_Log::debug(__CLASS__, "Plugin '{$plugin->getName()}' ({$plugin->getIdentifier()}) is in a process, sending message through socket.");

				// Remove the "on" from the start.
				$messageName  = substr($method, 2);
				$messageName  = lcfirst($messageName);
				$messageToken = $this->currentMessageToken;

				$pluginId = $plugin->getIdentifier();

				// If we know which socket the plugin lives behind, we provide the stream identifier.
				// Otherwise we just broadcast it to all sockets.
				if(isset($this->pluginSockets[$pluginId]))
				{
					$this->socketServer->appendOutgoingData(
						self::encodeMessage($messageName, $params, $messageToken, $this->currentContextPluginId, $pluginId),
						$this->pluginSockets[$pluginId]
					);
				}
				else
				{
					$this->socketServer->appendOutgoingData(
						self::encodeMessage($messageName, $params, $messageToken, $this->currentContextPluginId, $pluginId)
					);
				}
			}
			else
			{
				Ype_Log::debug("__CLASS__", "Plugin '{$plugin->getName()}' ({$plugin->getIdentifier()}) is not in a process.");

				$oldSenderPluginId            = $this->senderPluginId;
				$this->senderPluginId         = $this->currentContextPluginId;
				$this->currentContextPluginId = $plugin->getIdentifier();
				call_user_func_array($callback, $params);
				$this->currentContextPluginId = $this->senderPluginId;
				$this->senderPluginId         = $oldSenderPluginId;
			}
		}
		else
		{
			throw new Exception("Only callbacks to Plugins are supported for now.");
		}
	}

	public function onSocketServerConnect($streamIdentifier)
	{
		$this->lineBuffers[$streamIdentifier] = '';
	}

	/**
	 * @param string $line
	 * @param int    $streamIdentifier
	 */
	public function onSocketServerRead($line, $streamIdentifier)
	{
		$this->lineBuffers[$streamIdentifier] .= $line;
		if(substr($this->lineBuffers[$streamIdentifier], -1) === "\n")
		{
			list($messageName, $params, $messageToken, $senderId, $targetId) = self::decodeMessage($line);
			if(!isset($this->socketPlugins[$streamIdentifier][$senderId]))
			{
				$this->socketPlugins[$streamIdentifier][$senderId] = true;
				$this->pluginSockets[$senderId]                    = $streamIdentifier;
			}
			$this->sendMessage($messageName, $params, $targetId, $messageToken, $senderId);
		}
	}

	/**
	 * @param string $line
	 * @param string $streamIdentifier
	 */
	public function onProcessRead($line, $streamIdentifier)
	{
		if(isset($this->streamPluginIdentifiers[$streamIdentifier]))
		{
			$this->sendMessage('processStandardOutput', array($line), null, null, $this->streamPluginIdentifiers[$streamIdentifier]);
		}
		else
		{
			Ype_Log::warning('Broker_Internal', "Missing Plugin for Stream '{$streamIdentifier}'");
		}
	}

	/**
	 * @param string $line
	 * @param string $streamIdentifier
	 */
	public function onProcessError($line, $streamIdentifier)
	{
		if(isset($this->streamPluginIdentifiers[$streamIdentifier]))
		{
			$this->sendMessage('processStandardError', array($line), null, null, $this->streamPluginIdentifiers[$streamIdentifier]);
		}
		else
		{
			Ype_Log::warning('Broker_Internal', "Missing Plugin for Stream '{$streamIdentifier}'");
		}
	}

	public function closeAll()
	{
		foreach($this->processes as $process)
		{
			$process->close();
		}
		$this->socketServer->stop();
	}

	/**
	 * @return Ype_NonBlockingStream_Socket_Server
	 */
	private function _startSocketServer()
	{
		$socketFile = Ype::ensureConfigPath(getmypid() . '.socket');

		$socketServer = new Ype_NonBlockingStream_Socket_Server($this->streamHandler, $socketFile);
		$socketServer->registerConnectCallback(array($this, 'onSocketServerConnect'));
		$socketServer->registerReadCallback(array($this, 'onSocketServerRead'));
		$socketServer->run();

		return $socketServer;
	}

	/**
	 * @param Ype_ProcessCompatiblePluginInterface $plugin
	 * @return Ype_NonBlockingStream_Process
	 */
	private function _startPluginProcess(Ype_ProcessCompatiblePluginInterface $plugin)
	{
		Ype_Log::debugFunctionCall();

		$socketServer = $this->socketServer;

		$env       = array();
		$arguments = array(
			'--plugin',         $plugin->getName(),
			'--identifier',     $plugin->getIdentifier(),
			'--confdir',        Ype::getConfDir(),
			'--server-address', $socketServer->getAddress(),
			'--server-port',    $socketServer->getPort(),
		);

		// TODO temp debug stuff
//		if($plugin instanceof Ype_Plugin_TextCanvas)
//		{
//			$env['XDEBUG_CONFIG'] = 'idekey=YPE';
//		}

		$process = new Ype_NonBlockingStream_Process($this->streamHandler, YPE_EXECUTABLE, $arguments, null, $env);
		$process->registerReadCallback(array($this, 'onProcessRead'));
		$process->registerErrorCallback(array($this, 'onProcessError'));

		if($process->open())
		{
			$this->streamPluginIdentifiers[$process->getStdoutStreamIdentifier()] = $plugin->getIdentifier();
			$this->streamPluginIdentifiers[$process->getStderrStreamIdentifier()] = $plugin->getIdentifier();
			$this->streamPluginIdentifiers[$process->getStdinStreamIdentifier()]  = $plugin->getIdentifier();
			return $process;
		}
		else
		{
			Ype_Log::warning(__CLASS__, "Unable to start process for Plugin '{$plugin->getName()}' with identifier {$plugin->getIdentifier()}.");
		}

		return null;
	}

	public function quit()
	{
		$this->socketServer->stopListening();
	}
}