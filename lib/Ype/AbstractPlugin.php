<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2015 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype
 * @author	  Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2015 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/**
 * @package Ype
 */
abstract class Ype_AbstractPlugin implements Ype_PluginInterface
{
	/** @var int */
	private static $_index = 0;
	/** @var array bool[] */
	private static $_identifiers = array();
	
	/** @var int */
	private $_identifier = null;
	/** @var string */
	private $_name = '';

	protected $isReady = false;

	protected $context;

	public function __destruct()
	{
		self::$_identifiers[$this->_identifier] = null;
		unset(self::$_identifiers[$this->_identifier]);
	}

	/**
	 * Mark this plugin as ready.
	 *
	 * Will announce the availability of this plugin to all other plugins.
	 */
	protected function ready()
	{
		if(!$this->isReady)
		{
			$this->isReady = true;
			Ype::message('pluginAvailable', array(get_class($this)));
		}
	}

	/**
	 * Called when this plugin has been registered by the broker.
	 *
	 * By default just announces that the Plugin is ready by calling $this->ready().
	 */
	public function onInitializePlugin()
	{
		$this->ready();
	}

	public function onPluginRegistered($pluginClass = null)
	{
		if($this->isReady)
		{
			// Let any new Plugins know that this Plugin is also ready.
			Ype::message('pluginAvailable', array(get_class($this)), Ype::getBroker()->getSenderPluginId());
		}
	}

	/**
	 * @param string $message
	 */
	public function onQuit($message = '')
	{
		Ype::message('pluginQuit', array($this->getIdentifier()));
	}

	/**
	 * @param int $identifier
	 * @throws Exception
	 */
	public function setIdentifier($identifier)
	{
		if($this->_identifier == null)
		{
			if(isset(self::$_identifiers[$identifier]))
			{
				throw new Exception("Plugin identifier '{$identifier}' already exists.");
			}

			self::$_identifiers[$this->_identifier] = null;
			unset(self::$_identifiers[$this->_identifier]);
			self::$_identifiers[$identifier] = true;
			
			$this->_identifier = $identifier;
		}
		else
		{
			throw new Exception("Plugin identifier already set for Plugin: '{$this->_identifier}' where trying to set identifier" .
			                    "'{$identifier}'");
		}
	}

	/**
	 * @return int
	 */
	public function getIdentifier()
	{
		if($this->_identifier === null)
		{
			do
			{
				$identifier = ++self::$_index;
			}
			while(isset(self::$_identifiers[$identifier]));

			self::$_identifiers[$identifier] = true;
			$this->_identifier = $identifier;
		}
		
		return $this->_identifier;
	}

	/**
	 * @return string
	 * @throws Exception
	 */
	public function getName()
	{
		if($this->_name === '')
		{
			$pluginClassName = get_class($this);
			$this->_name = preg_replace('/^(Ype_Plugin_)/', '', $pluginClassName, 1);

			if($this->_name === '')
			{
				throw new Exception("Couldn't get name for Plugin '{$pluginClassName}'");
			}
		}

		return $this->_name;
	}
}