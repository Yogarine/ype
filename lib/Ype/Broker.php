<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2015 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype
 * @author	  Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2015 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/** We depend on lcfirst() */
require_once 'ype_compatibility.php';

/**
 * @package Ype
 */
abstract class Ype_Broker
{
	const ORIGIN_LOCAL = 'local';
	const ORIGIN_UPSTREAM = 'upstream';
	
	/** @var Ype_NonBlockingStream_Handler */
	protected $streamHandler;

	/** @var array e.g. array('messageName' => array('key' => array($object, 'onMessage'))) */
	protected $messageCallbacks = array();

	/** @var array e.g. array($pluginId => array('messageName' => array(0 => array('foo', 'default')))) */
	protected $namedMessageParameters = array();

	/** @var int Keeps track at the Plugin that's the current context. */
	protected $currentContextPluginId = null;

	/** @var int Keeps track of the what is the Plugin that sent the message we're currently handling. */
	protected $senderPluginId = null;

	/** @var int Keeps track of the current specific message we're handling. */
	protected $currentMessageToken = null;

	/** @var int Keeps track of the current specific message we're handling. */
	protected $messageTokenIndex = null;

	/** @var Ype_AbstractPlugin[] */
	protected $plugins = array();

	/**
	 * Constructor.
	 */
	public function __construct()
	{
		$this->streamHandler = new Ype_NonBlockingStream_Handler();
	}

	/**
	 * @param callable $callback
	 * @param array    $params
	 * @param int      $contextPluginId
	 */
	abstract protected function callback($callback, $params, $contextPluginId);

	abstract public function closeAll();

	abstract public function quit();

	public function getCurrentMessageToken()
	{
		return $this->currentMessageToken;
	}

	public function getCurrentContextPluginId()
	{
		return $this->currentContextPluginId;
	}

	public function getSenderPluginId()
	{
		return $this->senderPluginId;
	}

	/**
	 * Sends a message.
	 *
	 * Important: This function should be as efficient as possible, since it potentially gets called quite frequently.
	 *
	 * @param string     $messageName
	 * @param array      $namedParams
	 * @param int|string $target
	 * @param int        $messageToken
	 * @param int        $senderId
	 * @return int
	 */
	public function sendMessage($messageName, array $namedParams = array(), $target = null, $messageToken = null,
	                            $senderId = null)
	{
		if(!isset($this->messageCallbacks[$messageName]))
		{
			return null;
		}

		/** @var callable[] $callbacks */
		$callbacks = $this->messageCallbacks[$messageName];

		if(null === $messageToken)
		{
			$messageToken = ++$this->messageTokenIndex;
		}
		$this->currentMessageToken = $messageToken;
		$called = false;

		if(null !== $senderId)
		{
			// Manually override the plugin context.
			$oldContextPluginId = $this->currentContextPluginId;
			$this->currentContextPluginId = $senderId;
		}

		foreach($callbacks as $pluginId => $callback)
		{
			if(is_object($callback[0]))
			{
				$callbackClass = get_class($callback[0]);
			}
			else
			{
				$callbackClass = $callback[0];
			}

			if($target === null || $target == $callbackClass || $target == $pluginId)
			{
				$params = array();
				foreach($this->namedMessageParameters[$pluginId][$messageName] as $index => $item)
				{
					if(isset($namedParams[$item[0]]))
					{
						$params[$index] = $namedParams[$item[0]];
					}
					elseif(isset($namedParams[$index]))
					{
						$params[$index] = $namedParams[$index];
					}
					else
					{
						$params[$index] = $item[1];
					}
				}

				$this->callback($callback, $params, $pluginId);
				$called = true;
			}
		}

//		$pluginNames = array();
//		foreach($this->plugins as $plugin)
//		{
//			$pluginNames[] = $plugin->getName();
//		}
//		Ype_Log::debug(__CLASS__, $pluginNames);

		if(null !== $senderId)
		{
			/** @noinspection PhpUndefinedVariableInspection We know $currentSenderId is set if $senderId isn't null. */
			$this->currentContextPluginId = $oldContextPluginId;
		}

		if($called)
		{
			return $messageToken;
		}

		return null;
	}

	/**
	 * @param Ype_AbstractPlugin $plugin
	 * @return boolean True iff registration was successful.
	 */
	public function registerPlugin(Ype_AbstractPlugin $plugin)
	{
		Ype_Log::debugFunctionCall();

		$registrationSuccessful = false;

		$object  = new ReflectionObject($plugin);
		$methods = $object->getMethods(ReflectionMethod::IS_PUBLIC);

		foreach($methods as $method)
		{
			if(!$method->isStatic() && preg_match('/^on[A-Z]/', $method->getName()))
			{
				$messageName = substr($method->getName(), 2);
				$messageName = lcfirst($messageName);
				$this->setMessageCallback($messageName, $plugin);
				$this->setNamedMessageParameters($messageName, $method, $plugin);

				$registrationSuccessful = true;
			}
		}

		if($registrationSuccessful)
		{
			$this->plugins[$plugin->getIdentifier()] = $plugin;
		}

		return $registrationSuccessful;
	}

	public function unregisterPlugin($pluginId)
	{
		Ype_Log::debugFunctionCall();

		$this->removeMessageCallbacks($pluginId);
		$this->removeNamedMessageParameters($pluginId);
		unset($this->plugins[$pluginId]);
	}

	/**
	 * @param string             $messageName
	 * @param ReflectionMethod   $method
	 * @param Ype_AbstractPlugin $plugin
	 */
	public function setNamedMessageParameters($messageName, ReflectionMethod $method, Ype_AbstractPlugin $plugin)
	{
		$pluginId = $plugin->getIdentifier();

		if(!isset($this->namedMessageParameters[$pluginId]))
		{
			$this->namedMessageParameters[$pluginId] = array();
		}

		if(!isset($this->namedMessageParameters[$pluginId][$messageName]))
		{
			$this->namedMessageParameters[$pluginId][$messageName] = array();
		}

		foreach($method->getParameters() as $parameter)
		{
			if($parameter->isOptional())
			{
				$defaultValue = $parameter->getDefaultValue();
			}
			else
			{
				$defaultValue = null;
			}

			$this->namedMessageParameters[$pluginId][$messageName][$parameter->getPosition()] = array(
				$parameter->getName(),
				$defaultValue,
			);
		}
	}

	public function removeNamedMessageParameters($pluginId)
	{
		Ype_Log::debugFunctionCall();

		unset($this->namedMessageParameters[$pluginId]);
	}

	/**
	 * @param string             $messageName
	 * @param Ype_AbstractPlugin $plugin
	 */
	public function setMessageCallback($messageName, Ype_AbstractPlugin $plugin)
	{
		$pluginId = $plugin->getIdentifier();
		$method   = 'on' . ucfirst($messageName);

		$this->messageCallbacks[$messageName][$pluginId] = array($plugin, $method);
	}

	public function removeMessageCallbacks($pluginId)
	{
		foreach($this->messageCallbacks as $messageName => $pluginCallbacks)
		{
			if(isset($this->messageCallbacks[$messageName][$pluginId]))
			{
				unset($this->messageCallbacks[$messageName][$pluginId]);
			}
		}
	}

	/**
	 * @return Ype_NonBlockingStream_Handler
	 */
	public function getStreamHandler()
	{
		return $this->streamHandler;
	}

	/**
	 * @param string  $messageName
	 * @param array   $params
	 * @param int     $targetId
	 * @param int     $senderId
	 * @param int     $messageToken
	 * @return string
	 */
	public static function encodeMessage($messageName, $params, $messageToken, $senderId, $targetId = null)
	{
		$strings = array(
			$messageName,
			base64_encode(serialize($params)),
			base64_encode(serialize($messageToken)),
			base64_encode(serialize($senderId)),
			base64_encode(serialize($targetId)),
		);
		$result = implode(',', $strings) . PHP_EOL;

		return $result;
	}

	/**
	 * @param string $buffer
	 * @return array
	 * @throws Exception
	 */
	public static function decodeMessage($buffer)
	{
		$buffer = trim($buffer);
		$data   = explode(',', $buffer, 5);

		if(count($data) != 5)
		{
			throw new Exception("Invalid data: '{$buffer}'");
		}

		$messageName  = $data[0];
		$params       = unserialize(base64_decode($data[1]));
		$messageToken = unserialize(base64_decode($data[2]));
		$senderId     = unserialize(base64_decode($data[3]));
		$targetId     = unserialize(base64_decode($data[4]));

		return array($messageName, $params, $messageToken, $senderId, $targetId);
	}

	/**
	 * Run this plugin.
	 * @param int $timeout
	 * @return bool
	 */
	public function wait($timeout = null)
	{
		return $this->streamHandler->waitForStreams($timeout);
	}
}