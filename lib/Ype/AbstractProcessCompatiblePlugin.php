<?php

/**
 * @author Alwin Garside <alwin@garsi.de>
 */
abstract class Ype_AbstractProcessCompatiblePlugin extends Ype_AbstractPlugin implements Ype_ProcessCompatiblePluginInterface
{
	/**
	 * @param string $message
	 */
	public function onQuit($message = '')
	{
		Ype::quit($message);
		Ype::message('pluginQuit', array($this->getIdentifier()));
		Ype::getBroker()->unregisterPlugin($this->getIdentifier());
	}

	public function onSessionTextBoxCreated($textBoxId)
	{
		Ype::message('setDefaultTextBox', array($textBoxId));
	}
}