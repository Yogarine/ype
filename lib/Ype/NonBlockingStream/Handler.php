<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2016 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype\NonBlockingStream
 * @author    Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2016 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/**
 * @package Ype\NonBlockingStream
 */
class Ype_NonBlockingStream_Handler
{
	/** @var resource[] */
	protected $readStreams = array();
	/** @var resource[] */
	protected $writeStreams = array();

	/** @var callable[] */
	protected $readCallbacks = array();
	/** @var callable[] */
	protected $writeCallbacks = array();
	/** @var callable[] */
	protected $timeoutCallbacks = array();

	/** @var float[] */
	protected $timeoutTimes = array();

	/** @var array */
	protected $verboseItems = null;

	/**
	 * @param  resource $stream
	 * @param  callable $callback
	 * @param  callable $timeoutCallback
	 * @param  int      $timeoutDuration
	 * @throws Exception
	 */
	public function registerReadStream($stream, $callback, $timeoutCallback = null, $timeoutDuration = 5)
	{
		Ype_Log::debugFunctionCall();

		if(false == is_resource($stream))
		{
			throw new Exception("Invalid stream provided");
		}

		$key                       = (string)$stream;
		$this->readStreams[$key]   = $stream;
		$this->readCallbacks[$key] = $callback;

		if($timeoutCallback)
		{
			$this->registerTimeout($key, $timeoutCallback, $timeoutDuration);
		}
		else
		{
			$this->unregisterTimeout($key);
		}
	}

	/**
	 * @return boolean
	 */
	public function hasStreams()
	{
		return $this->readStreams || $this->writeStreams;
	}

	/**
	 * @param  string    $key
	 * @param  callable  $callback
	 * @param  int       $timeoutDuration
	 */
	public function registerTimeout($key, $callback, $timeoutDuration = 5)
	{
		$this->timeoutCallbacks[$key] = $callback;
		$this->timeoutTimes[$key] = array(microtime(true), $timeoutDuration);
	}

	/**
	 * @param  string  $key
	 */
	public function unregisterTimeout($key)
	{
		Ype_Log::debugFunctionCall();

		unset($this->timeoutCallbacks[$key]);
		unset($this->timeoutTimes[$key]);
	}

	/**
	 * @param  resource $stream
	 */
	public function unregisterReadStream($stream)
	{
		Ype_Log::debugFunctionCall();

		$key = (string)$stream;
		unset($this->readStreams[$key]);
		unset($this->readCallbacks[$key]);
		$this->unregisterTimeout($key);
	}

	/**
	 * @param  resource $stream
	 * @param  callable $callback
	 */
	public function registerWriteStream($stream, $callback)
	{
		Ype_Log::debugFunctionCall();

		$key                        = (string)$stream;
		$this->writeStreams[$key]   = $stream;
		$this->writeCallbacks[$key] = $callback;
	}

	/**
	 * @param resource|string $stream
	 */
	public function unregisterWriteStream($stream)
	{
		$key = (string) $stream;
		unset($this->writeStreams[$key]);
		unset($this->writeCallbacks[$key]);
	}

	/**
	 * @param  int $timeout [optional]
	 * @return bool
	 */
	public function waitForStreams($timeout = null)
	{
		Ype_Log::debugFunctionCall(false == $this->readStreams && false == $this->writeStreams);
//		foreach($this->readStreams as $readStream)
//		{
//			Ype_Log::debug(__CLASS__, "Read stream: '" . $readStream . "', ");
//			Ype_Log::debugCallback(__CLASS__, $this->readCallbacks[(string) $readStream]);
//		}
//
//		foreach($this->writeStreams as $writeStream)
//		{
//			Ype_Log::debug(__CLASS__, "Read stream: '" . $writeStream . "', ");
//			Ype_Log::debugCallback(__CLASS__, $this->writeStreams[(string) $writeStream]);
//		}

		if(false == $this->readStreams && false == $this->writeStreams)
		{
			return false;
		}
		
		$start = microtime(true);

		if(null === $timeout && count($this->timeoutCallbacks) > 0)
		{
			$timeout = 60; // our maximum timeout value
			foreach($this->timeoutTimes as $timeoutTime)
			{
				list($timeoutStart, $duration) = $timeoutTime;
				$toGo = $duration - ($start - $timeoutStart);
				$toGo += 0.005;
				$timeout = min($timeout, $toGo);
			}

			$timeout = max(0.05, $timeout);
		}

		if(null === $timeout)
		{
			$timeoutSec = null;
			$timeoutMicroSec = null;
		}
		else
		{
			$timeoutSec = (int)$timeout;
			$timeoutMicroSec = ($timeout - $timeoutSec) * 1000 * 1000;
		}

		$read  = $this->readStreams;
		$write = $this->writeStreams;

		$writeStreamCount = 0;
		if($write)
		{
			$read_null = null;
			$except    = null;
			Ype_Log::debug(__CLASS__, 'checking for write streams ' . $timeout .  ' (' . count($write) . ' sockets)');
			/** @noinspection PhpParamsInspection */
			$writeStreamCount = stream_select($read_null, $write, $except, 0);
		}

		$readStreamCount = 0;
		if($read)
		{
			$write_null = null;
			$except     = null;
			Ype_Log::debug(__CLASS__, 'checking for read streams ' . $timeout .  ' (' . count($read) . ' sockets)');
			/** @noinspection PhpParamsInspection */
			$readStreamCount = stream_select($read, $write_null, $except, 0);
		}

		$streamCount = $writeStreamCount + $readStreamCount;

		if(0 == $streamCount)
		{
			$read   = $this->readStreams;
			$write  = $this->writeStreams;
			$except = null;
			Ype_Log::debug(__CLASS__, 'waiting for streams ' . $timeout .  ' (' . count($read) . ' read sockets, ' . count($read) .
			                          ' write sockets)');

			/** @noinspection PhpParamsInspection */
			$streamCount = stream_select($read, $write, $except, $timeoutSec, $timeoutMicroSec);
		}

		Ype_Log::debug(__CLASS__, 'result: ' . count($read) . ' read, ' . count($write) . ' write');

		if(false === $streamCount)
		{
			foreach($this->writeStreams as $index => $stream)
			{
				if(!is_resource($stream))
				{
					Ype_Log::warning(__CLASS__, "Removing write stream '{$index}' because it became invalid");
					unset($this->writeStreams[$index]);
				}
			}

			foreach($this->readStreams as $index => $stream)
			{
				if(!is_resource($stream))
				{
					Ype_Log::warning(__CLASS__, "Removing read stream '{$index}' because it became invalid");
					unset($this->readStreams[$index]);
				}
			}
		}

		$end = microtime(true);

		foreach($this->timeoutTimes as $key => $timeoutTime)
		{
			list ($timeoutStart, $duration) = $timeoutTime;

			$waitingFor = $end - $timeoutStart;
			if($waitingFor > $duration)
			{
				$this->timeoutTimes[$key][0] = $end;
				$stream = isset($this->readStreams[$key]) ? $this->readStreams[$key] : $key;
				$callback = $this->timeoutCallbacks[$key];
				if($callback !== null)
				{
					call_user_func($callback, $stream);
				}
				else
				{
					Ype_Log::warning(__CLASS__, "Missing callback for timeout '{$key}'");
				}
			}
		}

		if($streamCount)
		{
			$this->handleWriteStreams($write);
			$this->handleReadStreams($read);
		}

		return $this->readStreams || $this->writeStreams;
	}

	/**
	 * @param  resource[]  $read
	 */
	protected function handleReadStreams(array $read)
	{
		foreach($read as $stream)
		{
			$key = (string) $stream;
			if(isset($this->readCallbacks[$key]))
			{
				// XXX debug stuff
				$callback = $this->readCallbacks[$key];
				$object = $callback[0];
				$method = $callback[1];

				if(is_object($object))
				{
					$object = get_class($object);
				}

				Ype_Log::debug(__CLASS__, "Calling read callback {$object}::{$method}() for '{$key}'.");
				call_user_func($this->readCallbacks[$key], $stream);
			}
		}
	}

	/**
	 * @param  resource[]  $write
	 */
	protected function handleWriteStreams(array $write)
	{
		foreach($write as $stream)
		{
			$key = (string) $stream;
			if(isset($this->writeCallbacks[$key]))
			{
				// XXX debug stuff
				$callback = $this->writeCallbacks[$key];
				$object = $callback[0];
				$method = $callback[1];

				if(is_object($object))
				{
					$object = get_class($object);
				}

				Ype_Log::debug(__CLASS__, "Calling write callback {$object}::{$method}() for '{$key}'.");

				call_user_func($this->writeCallbacks[$key], $stream);
			}
		}
	}
}