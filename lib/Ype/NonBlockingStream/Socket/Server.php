<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2016 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype\NonBlockingStream\Socket
 * @author    Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2016 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/** This file depends on AF_UNIX. */
require_once 'ype_compatibility.php';

/**
 * @package Ype\NonBlockingStream\Socket
 */
class Ype_NonBlockingStream_Socket_Server extends Ype_NonBlockingStream_Socket_Client
{
	/** @var callable[] */
	protected $connectCallbacks = array();

	public function __destruct()
	{
		if(AF_UNIX == $this->domain && is_file($this->address))
		{
			unlink($this->address);
		}
	}

	public function run()
	{
		Ype_Log::debugFunctionCall();

		if(null === $this->socket)
		{
			$localSocket = $this->getSocketUri();
			Ype_Log::debug(__CLASS__, "socketUri: '{$localSocket}'");

			$socket = stream_socket_server($localSocket, $errno, $errstr);
			if(false == is_resource($socket))
			{
				if(0 == $errno)
				{
					throw new Exception("Failed to initialize server socket on '{$localSocket}'");
				}
				else
				{
					throw new Exception("Failed to create socket server on '{$localSocket}: '{$errstr}' ({$errno})");
				}
			}

			if(false == stream_set_blocking($socket, false))
			{
				throw new Exception("Failed to set server socket '{$socket}' to non-blocking (connected to '{$localSocket}')");
			}

			Ype_Log::debug(__CLASS__, "Running socket server on '{$localSocket}'");

			$this->socket = $socket;
			$this->handler->registerReadStream($this->socket, array($this, 'onAccept'));
		}
	}

	/**
	 * @param resource $socket
	 */
	public function onAccept($socket)
	{
		Ype_Log::debugFunctionCall();

		$clientSocket = stream_socket_accept($socket, null, $peername);
		if(false == is_resource($clientSocket))
		{
			Ype_Log::error(__CLASS__, "Failed to accept connection on server socket '{$socket}' ({$this->getSocketUri()}), " .
			                          "from peer '{$peername}'");
		}
		else
		{
			$socketIdentifier = (string) $clientSocket;

			$socketReader = new Ype_NonBlockingStream_Reader($clientSocket, $this->handler);
			$socketReader->setReadMode($this->readMode);
			$socketReader->registerReadCallback(array($this, 'onRead'));
			$socketReader->registerDisconnectCallback(array($this, 'onReaderDisconnect'));
			$socketReader->registerForRead();
			$this->streamReaders[$socketIdentifier] = $socketReader;

			$socketWriter = new Ype_NonBlockingStream_Writer($clientSocket, $this->handler);
			$socketWriter->registerDisconnectCallback(array($this, 'onWriterDisconnect'));
			$this->streamWriters[$socketIdentifier] = $socketWriter;

			foreach($this->connectCallbacks as $connectCallback)
			{
				call_user_func($connectCallback, $socketIdentifier);
			}
		}
	}

	public function onReaderDisconnect($socketIdentifier)
	{
		unset($this->streamReaders[$socketIdentifier]);
	}

	public function onWriterDisconnect($socketIdentifier)
	{
		unset($this->streamWriters[$socketIdentifier]);
	}

	/**
	 * @param callable $callback
	 */
	public function registerConnectCallback($callback)
	{
		$this->connectCallbacks[] = $callback;
	}
}