<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2016 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype\NonBlockingStream\Socket
 * @author    Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2016 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/** This file depends on AF_UNIX. */
require_once 'ype_compatibility.php';

/**
 * @package Ype\NonBlockingStream\Socket
 */
class Ype_NonBlockingStream_Socket_Client
{
	const TRANSPORT_UNIX = 'unix';
	const TRANSPORT_TCP  = 'tcp';

	const CONNECTION_TIMEOUT = 5;

	/** @var Ype_NonBlockingStream_Handler */
	protected $handler;

	/** @var string If the socket is of the AF_INET family, the address is an IP in dotted-quad notation
	 *              (e.g. 127.0.0.1).
	 *              If the socket is of the AF_UNIX family, the address is the path of a Unix-domain socket
	 *              (e.g. /tmp/my.sock). */
	protected $address;

	/** @var int The port parameter is only used when binding an AF_INET socket,
	 *           and designates the port on which to listen for connections. */
	protected $port;

	/** @var resource A socket resource created with socket_create(). */
	protected $socket = null;

	/** @var int */
	protected $domain;

	/** @var int */
	protected $type;

	/** @var int */
	protected $protocol;

	/** @var string */
	protected $transport;

	/** @var Ype_NonBlockingStream_Reader[]  */
	protected $streamReaders = array();

	/** @var Ype_NonBlockingStream_Writer[]  */
	protected $streamWriters = array();

	/** @var callable[] */
	protected $readCallbacks = array();

	/** @var callable[] */
	protected $disconnectCallbacks = array();

	protected $readMode;

	/**
	 * @param Ype_NonBlockingStream_Handler $handler
	 * @param string                        $address           If the socket is of the AF_INET family, the address is an IP in dotted-quad
	 *                                                         notation (e.g. 127.0.0.1).
	 *                                                         If the socket is of the AF_UNIX family, the address is the path of a
	 *                                                         Unix-domain socket (e.g. /tmp/my.sock).
	 *
	 * @param int                           $port              The port parameter is only used when binding an AF_INET socket, and designates
	 *                                                         the port on which to listen for connections.
	 *
	 * @param int                           $domain            The domain parameter specifies the protocol family to be used by the socket.
	 *                                                         AF_INET  IPv4 Internet based protocols.
	 *                                                         TCP and UDP are common protocols of this protocol family.
	 *                                                         AF_INET6 IPv6 Internet based protocols.
	 *                                                         TCP and UDP are common protocols of this protocol family.
	 *                                                         AF_UNIX  Local communication protocol family.
	 *                                                         High efficiency and low overhead make it a great form of IPC
	 *                                                         (Interprocess Communication).
	 *
	 * @param int                           $type              The type parameter selects the type of communication to be used by the socket.
	 *
	 * @param int                           $protocol          The protocol parameter sets the specific protocol within the specified domain
	 *                                                         to be used when communicating on the returned socket.
	 *                                                         The proper value can be retrieved by name by using getprotobyname(). If the
	 *                                                         desired protocol is TCP, or UDP the corresponding constants SOL_TCP, and
	 *                                                         SOL_UDP can also be used.
	 *
	 * @param string                        $transport
	 * @param string                        $readMode          Whether to read streams until EOL or EOF.
	 */
	public function __construct(Ype_NonBlockingStream_Handler $handler, $address, $port = null, $domain = AF_UNIX,
	                            $type = SOCK_STREAM, $protocol = 0, $transport = 'unix',
	                            $readMode = Ype_NonBlockingStream_Reader::READ_MODE_EOL)
	{
		Ype_Log::debugFunctionCall();

		$this->handler  = $handler;
		$this->address  = $address;
		$this->port     = $port;
		$this->domain   = $domain;
		$this->type     = $type;
		$this->protocol = $protocol;
		$this->transport = $transport;
		$this->readMode  = $readMode;
	}

	/**
	 * Destructor makes sure the stop() method is called so all the sockets get closed gracefully.
	 */
	public function __destruct()
	{
		$this->stop();
	}

	protected function getSocketUri()
	{
		// TODO add support for more domains here and clean up a bit
		switch($this->domain)
		{
			case AF_UNIX:
				$transport = self::TRANSPORT_UNIX;
				$target    = $this->address;
				$port      = null;
				break;
			case AF_INET:
			case AF_INET6:
				$transport = $this->transport;
				$target    = $this->address;
				$port      = $this->port;
				break;

			default:
				throw new Exception("Unsupported socket domain: '{$this->domain}'");
		}

		$uri = "{$transport}://{$target}";
		if(null !== $port)
		{
			$uri .= ":{$port}";
		}

		return $uri;
	}

	/**
	 * Connects this client to the socket url.
	 */
	public function connect()
	{
		Ype_Log::debugFunctionCall();

		if(null === $this->socket)
		{
			$remoteSocket = $this->getSocketUri();
			$socket       = stream_socket_client($remoteSocket, $errno, $errstr);
			Ype_Log::debug(__CLASS__, "Connected to '{$remoteSocket}': '{$socket}'");

			if(false == is_resource($socket))
			{
				if(0 == $errno)
				{
					throw new Exception("Failed to initialize client socket to '{$remoteSocket}'");
				}
				else
				{
					throw new Exception("Failed to connect to '{$remoteSocket}': '{$errstr}' ({$errno})");
				}
			}

			if(false == stream_set_blocking($socket, false))
			{
				throw new Exception("Failed to set client socket '{$socket}' to non-blocking (connected to '{$remoteSocket}')");
			}

			$this->socket = $socket;

			$streamReader = new Ype_NonBlockingStream_Reader($this->socket, $this->handler);
			$streamReader->setReadMode($this->readMode);
			$streamReader->registerReadCallback(array($this, 'onRead'));
			$streamReader->registerDisconnectCallback(array($this, 'onReaderDisconnect'));
			$streamReader->registerForRead();
			$this->streamReaders[$streamReader->getStreamIdentifier()] = $streamReader;

			$streamWriter = new Ype_NonBlockingStream_Writer($this->socket, $this->handler);
			$streamWriter->registerDisconnectCallback(array($this, 'onWriterDisconnect'));
			$this->streamWriters[$streamWriter->getStreamIdentifier()] = $streamWriter;
		}
	}

	public function stopListening()
	{
		$this->handler->unregisterReadStream($this->socket);
	}

	/**
	 * Stops this client.
	 *
	 * Will unregister all the sockets from the handler and close all the sockets.
	 */
	public function stop()
	{
		if(null !== $this->socket)
		{
			$this->handler->unregisterReadStream($this->socket);
			fclose($this->socket);
			$this->socket = null;
		}

		foreach($this->streamReaders as $index => $clientReader)
		{
			$clientReader->close();
			unset($this->streamReaders[$index]);
		}
		$this->streamReaders = array();

		foreach($this->streamWriters as $index => $clientWriter)
		{
			$clientWriter->close();
			unset($this->streamWriters[$index]);
		}
		$this->streamWriters = array();
	}

	/**
	 * @param string $data
	 * @param string $targetSocketIdentifier
	 */
	public function appendOutgoingData($data, $targetSocketIdentifier = null)
	{
		if(null === $targetSocketIdentifier)
		{
			foreach($this->streamWriters as $clientWriter)
			{
				$clientWriter->appendOutgoingData($data);
			}
		}
		elseif(isset($this->streamWriters[$targetSocketIdentifier]))
		{
			$this->streamWriters[$targetSocketIdentifier]->appendOutgoingData($data);
		}
		else
		{
			Ype_Log::warning(__CLASS__, "Attempting to output to unknown connection: {$targetSocketIdentifier}");
		}
	}

	/**
	 * @param callable $callback
	 */
	public function registerReadCallback($callback)
	{
		$this->readCallbacks[] = $callback;
	}

	/**
	 * @param callable $callback
	 */
	public function registerDisconnectCallback($callback)
	{
		$this->disconnectCallbacks[] = $callback;
	}

	/**
	 * @param string $line
	 * @param string $socketIdentifier
	 */
	public function onRead($line, $socketIdentifier)
	{
		foreach($this->readCallbacks as $readCallback)
		{
			call_user_func($readCallback, $line, $socketIdentifier);
		}
	}

	/**
	 * @return string
	 */
	public function getAddress()
	{
		return $this->address;
	}

	/**
	 * @return int
	 */
	public function getPort()
	{
		return $this->port;
	}

	public function onReaderDisconnect($socketIdentifier)
	{
		unset($this->streamReaders[$socketIdentifier]);
		if(isset($this->streamWriters[$socketIdentifier]))
		{
			unset($this->streamWriters[$socketIdentifier]);
			foreach($this->disconnectCallbacks as $callback)
			{
				call_user_func($callback, $socketIdentifier);
			}
		}

		if(empty($this->streamReaders))
		{
			$this->stop();
		}
	}

	public function onWriterDisconnect($socketIdentifier)
	{
		unset($this->streamWriters[$socketIdentifier]);
		if(isset($this->streamReaders[$socketIdentifier]))
		{
			unset($this->streamReaders[$socketIdentifier]);
			foreach($this->disconnectCallbacks as $callback)
			{
				call_user_func($callback, $socketIdentifier);
			}
		}

		if(empty($this->streamReaders))
		{
			$this->stop();
		}

	}

	public function getStreamReader($streamIdentifier)
	{
		if(isset($this->streamReaders[$streamIdentifier]))
		{
			return $this->streamReaders[$streamIdentifier];
		}

		return null;
	}

	public function getStreamWriter($streamIdentifier)
	{
		if(isset($this->streamWriters[$streamIdentifier]))
		{
			return $this->streamWriters[$streamIdentifier];
		}

		return null;
	}
}