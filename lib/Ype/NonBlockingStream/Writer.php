<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2016 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype\NonBlockingStream
 * @author	  Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2016 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/**
 * @package Ype\NonBlockingStream
 */
class Ype_NonBlockingStream_Writer extends Ype_NonBlockingStream_Wrapper
{
	const DEFAULT_WRITE_BUFFER = 1024;

	const DEFAULT_MAX_RETRIES = 3;

	protected $writeBufferLength = self::DEFAULT_WRITE_BUFFER;

	protected $maxRetries = self::DEFAULT_MAX_RETRIES;

	protected $retryCount = 0;

	/** @var string */
	protected $outgoingData = '';

	public function __construct($stream, Ype_NonBlockingStream_Handler $handler)
	{
		parent::__construct($stream, $handler);

		stream_set_write_buffer($this->stream, $this->writeBufferLength);
	}

	public function __destruct()
	{
		$this->close();
	}

	protected function registerForWrite()
	{
		if($this->stream)
		{
			$this->handler->registerWriteStream($this->stream, array($this, 'write'));
		}
	}

	public function unregisterForWrite()
	{
		$this->handler->unregisterWriteStream($this->stream);
	}

	/**
	 * @param resource $stream
	 * @throws Exception
	 */
	public function write($stream)
	{
		$numWritten = 0;

		if('' !== $this->outgoingData)
		{
			$numWritten = fwrite($stream, $this->outgoingData, $this->writeBufferLength);
			if(false === $numWritten || 0 === $numWritten)
			{
				$this->callbackOnDisconnect($this->streamIdentifier);
				$this->close();
				throw new Exception("Failed to write to socket '{$stream}'");
			}

			$this->outgoingData = substr($this->outgoingData, $numWritten);
		}

		Ype_Log::debugFunctionCall("{$numWritten} bytes written");

		if(strlen($this->outgoingData) == 0)
		{
			$this->unregisterForWrite();
		}
	}

	/**
	 * @param string $data
	 */
	public function appendOutgoingData($data)
	{
		if(strlen($this->outgoingData) == 0)
		{
			$this->registerForWrite();
		}

		$this->outgoingData .= $data;
	}

	/**
	 * Close.
	 */
	public function close()
	{
		Ype_Log::debugFunctionCall($this->stream);

		if(is_resource($this->stream))
		{
			$this->handler->unregisterWriteStream($this->stream);
			fclose($this->stream);
		}
		else
		{
			$this->handler->unregisterWriteStream($this->streamIdentifier);
		}

		$this->stream = null;
		$this->streamIdentifier = null;
	}

	/**
	 * @return boolean
	 */
	public function writeIsDone()
	{
		return null == $this->stream || strlen($this->outgoingData) == 0;
	}
}
