<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2015 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype\Plugin
 * @author	  Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2015 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/**
 * @package Ype\Plugin
 */
class Ype_Plugin_StandardInput extends Ype_AbstractPlugin
{
	/** @var Ype_NonBlockingStream_Handler */
	protected $streamHandler;

	/**
	 * @param Ype_NonBlockingStream_Handler $streamHandler
	 */
	public function __construct(Ype_NonBlockingStream_Handler $streamHandler)
	{
		$streamHandler->registerReadStream(STDIN, array($this, 'readCallback'));
		$this->streamHandler = $streamHandler;
	}
	
	public function onQuit($message = '')
	{
		$this->streamHandler->unregisterReadStream(STDIN);

		parent::onQuit($message);
	}

	/**
	 * @param resource $stream
	 */
	public function readCallback($stream)
	{
		Ype_Log::debugFunctionCall();

		$char = false;
		while($char === false || ord($char) == 255)
		{
			$char = stream_get_contents($stream, 1);
		}

		if(ctype_cntrl($char))
		{
			Ype_Log::debug('Input', "char: <CC> (" . ord($char) . ")");
		}
		else
		{
			Ype_Log::debug('Input', "char: '{$char}' (" . ord($char) . ")");
		}

		$characterCode = ord($char);

		if($characterCode == Ype_ASCII::ESC)
		{
			$escapeSequence = self::getEscapeSequenceFromStream($stream);
			Ype::message('newEscapeSequence', array($escapeSequence), null, null, $this->getIdentifier());
		}
		else
		{
			Ype::message('newCharacterCode',  array($characterCode), null, null, $this->getIdentifier());
		}
	}

	protected static function getEscapeSequenceFromStream($stream)
	{
		$char = stream_get_contents($stream, 1);
		$sequence = $char;

		if (in_array($char, Ype_ASCII::$ESCAPE_SEQUENCE_CHARACTERS))
		{
			$char = stream_get_contents($stream, 1);
			$sequence .= $char;

			while (ord($char) < Ype_AnsiEscapeSequence::START_CSI_CODE || ord($char) > Ype_AnsiEscapeSequence::END_CSI_CODE)
			{
				$char = stream_get_contents($stream, 1);
				$sequence .= $char;
			}
		}

		return $sequence;
	}
}