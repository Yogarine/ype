<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2015 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype\Plugin
 * @author	  Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2015 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/**
 * @package Ype\Plugin
 */
class Ype_Plugin_SyntaxCompletion extends Ype_AbstractProcessCompatiblePlugin
{
	/** @var string[] */
	protected static $_completionCache = array();
	/** @var string */
	protected static $_currentCompletionWord = '';
	/** @var string */
	protected static $_currentCompletion = '';
	/** @var string */
	protected static $_currentCompletionSuffix = true;

	/**
	 * Updates the completion cache.
	 */
	public static function updateCompletionCache()
	{
		self::$_completionCache = array();

		// TODO get defined functions from Process
		$functions = get_defined_functions();
		foreach($functions as $completions)
		{
			foreach($completions as $completion)
			{
				self::$_completionCache[] = $completion . '()';
			}
		}

		$vars = get_defined_vars();
		foreach($vars as $type)
		{
			if(!is_array($type))
			{
				continue;
			}

			foreach($type as $scope)
			{
				foreach($scope as $variable)
				{
					self::$_completionCache[] = '$' . $variable;
				}
			}
		}
	}

	/**
	 * Add a completion to the completion cache.
	 *
	 * @param  string  $completion  Completion to add to the cache
	 */
	protected static function _addCompletion($completion)
	{
		if(!in_array($completion, self::$_completionCache))
		{
			self::$_completionCache[] = $completion;
		}
	}

	/**
	 * @fixme  Do proper caching
	 * @param  string    $search
	 * @param  string    $path
	 * @return string[]
	 */
	public static function searchDirectoriesWithPhpFilesInDir($search, $path)
	{
		$directories = array();

		// XXX do caching
		foreach(glob($path . DIRECTORY_SEPARATOR . "{$search}*") as $file)
		{
			if(is_dir($file))
			{
				$phpFiles = self::searchPhpFilesInDir('', $file);
				if(!empty($phpFiles))
				{
					$directories[] = $file;
				}
			}
		}

		return $directories;
	}

	/**
	 * @param  string    $search
	 * @param  string    $path
	 * @return string[]
	 */
	public static function searchPhpFilesInDir($search, $path)
	{
		$filenames = array();

		// XXX do caching
		foreach(glob($path . DIRECTORY_SEPARATOR . "$search*") as $file)
		{
			if(pathinfo($file, PATHINFO_EXTENSION) == 'php' && is_file($file))
			{
				$filenames[] = $file;
			}
		}

		return $filenames;
	}

	/**
	 * @param  string  $search
	 * @param  string  $filename
	 * @return array
	 */
	public static function searchClassesInPhpFile($search, $filename)
	{
		$contents = file_get_contents($filename);
		$matches  = array();
		if(preg_match_all('/class\s(' . preg_quote($search) . '[a-zA-Z0-9_]*)/i', $contents, $matches))
		{
			return $matches[1];
		}
		else
		{
			return array();
		}
	}


	/**
	 * @param  string  $search
	 * @return array
	 */
	public static function findCompletionsInIncludePaths($search)
	{
		if($search == '')
		{
			return array();
		}

		Ype_Log::debug('Ype', "Finding completions for '{$search}'");
		$classes = array();

		$prefixes  = array();
		$prefix    = '';
		$prefixDir = '';
		if(strpos($search, '_') !== false)
		{
			$prefixes  = explode('_', $search);
			$search    = array_pop($prefixes);
			$prefix    = implode('_', $prefixes) . '_';
			$prefixDir = implode(DIRECTORY_SEPARATOR, $prefixes);
		}

		Ype_Log::debug('Ype', "Prefixes: " . implode(',', $prefixes));
		Ype_Log::debug('Ype', "Prefix:   " . $prefix);
		Ype_Log::debug('Ype', "Prefix:   " . $prefixDir);

		$includePaths = self::getSession()->getIncludePaths();

		foreach($includePaths as $path)
		{
			if($prefixDir !== '')
			{
				$subPath = $path . DIRECTORY_SEPARATOR . $prefixDir;
				if(!file_exists($subPath))
				{
					continue;
				}

				$path  = realpath($subPath);
			}

			Ype_Log::debug('Ype', "    Search: " . $search);
			Ype_Log::debug('Ype', "    Path: " . $path);

			$files = self::searchPhpFilesInDir($search, $path);
			Ype_Log::debug('Ype', "    Files: " . implode(',', $files));
			foreach($files as $file)
			{
				$classes = array_merge($classes, self::searchClassesInPhpFile($prefix . $search, $file));
			}
			Ype_Log::debug('Ype', "    Classes: " . implode(',', $classes));

			$directories = self::searchDirectoriesWithPhpFilesInDir($search, $path);
			foreach($directories as $directory)
			{
				$classes[] = basename($directory) . '_';
			}
		}

		return array_unique($classes);
	}

	/**
	 * Get a word that is eligable for completion.
	 *
	 * @param  string  $left
	 * @param  string  $right
	 * @return string
	 */
	protected static function _getCompletionWord($left, $right)
	{
		Ype_Log::debug('Ype', "_getCompletionWord('{$left}', '{$right}')");

		if(preg_match('/(?:\s|^|=)(\$?\w+)$/i', $left, $matches))
		{
			return $matches[1];
		}

		return '';
	}

	/**
	 * Find completions for given text.
	 *
	 * @param $text
	 * @return array
	 */
	public static function findCompletions($text)
	{
		$text        = preg_quote($text);
		$completions = preg_grep("/^{$text}/i", self::$_completionCache);
		// XXX this should also be cached
		$completions = array_merge($completions, self::findCompletionsInIncludePaths($text));
		return array_values($completions);
	}

	/**
	 * @param  string    $text
	 * @return string[]
	 */
	public static function getCompletion($text)
	{
		$completions     = self::findCompletions($text);
		$completionCount = count($completions);
		$textLength      = strlen($text);

		Ype_Log::debug('Ype', "Found {$completionCount} completions for '{$text}'.");
		Ype_Log::verbose('Ype', "Completions:" . PHP_EOL . print_r($completions, true));

		if($completionCount == 1)
		{
			return array(substr($completions[0], $textLength), '');
		}
		elseif($completionCount == 0)
		{
			return array('', '');
		}
		else
		{
			$length     = strlen($completions[0]);
			$completion = '';

			// Figure out until which character the completions are equal.
			for($i = $textLength; $i <= ($length + 1); $i++)
			{
				$substr = substr($completions[0], 0, $i);
				$found  = preg_grep('/^' . preg_quote($substr) . '/i', $completions);

				if(count($found) == $completionCount)
				{
					$completion = $substr;
				}
			}

			$completion = substr($completion, $textLength);
			return array($completion, '...');
		}
	}

	protected function onNewCharacterCode($characterCode, $leftBuffer, $rightBuffer)
	{
		$completion        = '';
		$currentCompletion = $completion;
		$left = '';

		switch($characterCode)
		{
			// TAB character, do completion shizzle
			case Ype_ASCII::HT:
				$left .= $currentCompletion;
				break;
		}
	}

	/**
	 * Print the completion
	 *
	 * @param  string  $oldLeft
	 * @param  string  $oldRight
	 * @param  string  $newLeft
	 * @param  string  $newRight
	 * @return string
	 */
	protected static function _updateCompletion($oldLeft, $oldRight, $newLeft, $newRight)
	{
		$completionWord                      = self::_getCompletionWord($newLeft, $newRight);
		list($completion, $completionSuffix) = self::getCompletion($completionWord);

		Ype_Log::debug('updateCompletion', "old: " . self::$_currentCompletionWord . '|' . self::$_currentCompletion);
		Ype_Log::debug('updateCompletion', "new: {$completionWord}|{$completion}");

		$currentLeftPadding = strlen($oldLeft) - strlen(self::$_currentCompletionWord);
		$newLeftPadding     = strlen($newLeft) - strlen($completionWord);

		$currentAutoCompletion  = $currentLeftPadding > 0 ? str_repeat(' ', $currentLeftPadding) : '';
		$currentAutoCompletion .= self::$_currentCompletionWord . self::$_currentCompletion . self::$_currentCompletionSuffix;
		$newAutoCompletion      = $newLeftPadding > 0 ? str_repeat(' ', $newLeftPadding) : '';
		$newAutoCompletion     .= $completionWord . $completion . $completionSuffix;
		if($newAutoCompletion  == $completionWord)
		{
			$newAutoCompletion = '';
		}
		Ype_Log::debug('Ype', "currentAutoCompletion: '{$currentAutoCompletion}'");
		Ype_Log::debug('Ype', "newAutoCompletion:     '{$newAutoCompletion}'");

		if($currentAutoCompletion != $newAutoCompletion)
		{
			// Find the first position where the two differ.
			$diffPosition = strspn($currentAutoCompletion ^ $newAutoCompletion, "\0");
			Ype_Log::debug('Ype', "diffPosition: {$diffPosition}");

			// Check how much the cursor has changed since the last completion.
			$positionDiff = strlen($newLeft) - strlen($oldLeft);
			Ype_Log::debug('Ype', "positionDiff: {$positionDiff}");

			// Figure out how much we must position the cursor backwards.
			$backwardsAmount = strlen($newLeft) - $diffPosition;
			Ype_Log::debug('Ype', "backwardsAmount: {$backwardsAmount}");

			// Position the cursor at the correct location.
			Ype_ASCII::cursorUp(1);
			Ype_ASCII::cursorBackwards($backwardsAmount);

			// Write the new part of the completion
			$updatedText = substr($newAutoCompletion, $diffPosition);
			Ype_Log::debug('Ype', "updatedText: '{$updatedText}'");
			Ype_ASCII::setGraphicsRendition(array(
				Ype_ASCII::ESC_CSI_SGR_BOLD, Ype_ASCII::ESC_CSI_SGR_FOREGROUND_WHITE, Ype_ASCII::ESC_CSI_SGR_BACKGROUND_BLUE,
			));
			echo $updatedText;
			Ype_ASCII::setGraphicsRendition(array(Ype_ASCII::ESC_CSI_SGR_ALL_OFF));

			// Update the amount we should move the cursor backwards.
			$backwardsAmount = strlen($updatedText) - $backwardsAmount;

			// Figure out how much we need to clean up from the previos completion.
			$clearAmount = strlen(self::$_currentCompletion . self::$_currentCompletionSuffix) - $positionDiff - $backwardsAmount;

			if($clearAmount >= 0)
			{
				echo str_repeat(' ', $clearAmount);
			}
			else
			{
				$clearAmount = 0;
			}

			Ype_ASCII::cursorBackwards($backwardsAmount + $clearAmount);
			Ype_ASCII::cursorDown(1);
		}

		self::$_currentCompletion = $completion;
		if($newAutoCompletion == '')
		{
			self::$_currentCompletionWord = '';
		}
		else
		{
			self::$_currentCompletionWord = $completionWord;
		}

		self::$_currentCompletionSuffix = $completionSuffix;

		return $completion;
	}
}