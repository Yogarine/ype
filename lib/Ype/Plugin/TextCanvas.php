<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2015 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype\Plugin
 * @author	  Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2015 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/**
 * @package Ype\Plugin
 */
class Ype_Plugin_TextCanvas extends Ype_AbstractProcessCompatiblePlugin
{
	const MESSAGE_NAME_TEXT_BOX_CREATED        = 'textBoxCreated';
	const MESSAGE_NAME_TEXT_BOX_CREATE_SUCCESS = 'textBoxCreateSuccess';
	const DEFAULT_DISPLAY_ELEMENT_NAME         = 'Unnamed';

	public $width             = null;
	public $height            = null;

	/** @var Ype_Plugin_Screen_TextBox[] */
	protected $displayElements                = array();
	protected $displayElementIds              = array();
	protected $pluginDefaultDisplayElementIds = array();
	protected $buffer                         = array();
	/** @var Ype_Plugin_Screen_TextBox */
	protected $activeDisplayElement = null;

	public function onInitializePlugin()
	{
		if(!$this->isReady)
		{
			Ype::message('writeEscapeSequenceToStandardOutput', array(Ype_AnsiEscapeSequence::CSI_WM_REPORT_TEXT_AREA_SIZE));
			Ype::message('writeEscapeSequenceToStandardOutput', array(Ype_AnsiEscapeSequence::CSI_DSR_CPR));
			$csiEraseDisplayAll = Ype_AnsiEscapeSequence::getCsiEscapeSequence(
				Ype_AnsiEscapeSequence::CSI_ED,
				Ype_AnsiEscapeSequence::ED_ALL
			);
			Ype::message('writeEscapeSequenceToStandardOutput', array($csiEraseDisplayAll));

			$this->ready();
		}
	}

	public function onNewEscapeSequence($escapeSequence = '')
	{
		$pattern = Ype_AnsiEscapeSequence::getPatternForSequence($escapeSequence);

		switch($pattern)
		{
			case Ype_AnsiEscapeSequence::CSI_WM_RESIZE_TEXT_AREA:
				list($height, $width) = sscanf($escapeSequence, $pattern);
				if($this->height != $height || $this->width != $width)
				{
					$this->height = $height;
					$this->width  = $width;
					$this->draw(1, 1, $this->height, $this->width);
				}
				if(!$this->isReady)
				{
					$this->ready();
				}
				break;
			case Ype_AnsiEscapeSequence::CSI_CPR:
				list($this->cursorRow, $this->cursorColumn) = sscanf($escapeSequence, $pattern);
				break;
		}
	}

	public function draw($canvasDirtyStartRow = null, $canvasDirtyStartColumn = null, $canvasDirtyEndRow = 0, $canvasDirtyEndColumn = 0)
	{
		Ype_Log::debugFunctionCall();
		$canvasDirtyStartRow    = $canvasDirtyStartRow    ? $canvasDirtyStartRow : $this->height + 1;
		$canvasDirtyStartColumn = $canvasDirtyStartColumn ? $canvasDirtyStartColumn : $this->width + 1;

		foreach($this->displayElements as $displayElement)
		{
			$dimensions = self::_getScreenDimensions($displayElement);
			list($top, $left, $width, $height) = $dimensions;

			Ype_Log::debug("DIMENSIONS", $dimensions);

			// Make sure the scroll offset follows the cursor.
			if($displayElement->currentRow > $height)
			{
				$verticalScrollOffset = $displayElement->currentRow - $height;
			}
			else
			{
				$verticalScrollOffset = 0;
			}

			if($displayElement->currentColumn > $width)
			{
				$horizontalScrollOffset = $displayElement->currentColumn - $width;
			}
			else
			{
				$horizontalScrollOffset = 0;
			}

			Ype_Log::debug(__CLASS__, "verticalScrollOffset: {$verticalScrollOffset}");
			Ype_Log::debug(__CLASS__, "horizontalScrollOffset: {$horizontalScrollOffset}");

			Ype_Log::debug(__CLASS__, "\$displayElement->verticalScrollOffset: {$displayElement->verticalScrollOffset}");
			Ype_Log::debug(__CLASS__, "\$displayElement->verticalScrollOffset: {$displayElement->verticalScrollOffset}");

			if($displayElement->verticalScrollOffset   != $verticalScrollOffset ||
			   $displayElement->horizontalScrollOffset != $horizontalScrollOffset)
			{
				$displayElement->verticalScrollOffset   = $verticalScrollOffset;
				$displayElement->horizontalScrollOffset = $horizontalScrollOffset;
				$canvasDirtyStartRow    = min($canvasDirtyStartRow,    $top + 1);
				$canvasDirtyStartColumn = min($canvasDirtyStartColumn, $left + 1);
				$canvasDirtyEndRow      = max($canvasDirtyEndRow,      $top + $height);
				$canvasDirtyEndColumn   = max($canvasDirtyEndColumn,   $left + $width);
			}

			foreach($displayElement->dirtySections as $dirtySection)
			{
				Ype_Log::debug('DIRTY_SECTION', $dirtySection);

				list(
					$dirtyStartRow,
					$dirtyStartColumn,
					$dirtyEndRow,
					$dirtyEndColumn
				) = $dirtySection;

				// Offset the rows according to scroll position.
				$dirtyStartRow    -= $verticalScrollOffset;
				$dirtyStartColumn -= $horizontalScrollOffset;
				$dirtyEndRow      -= $verticalScrollOffset;
				$dirtyEndColumn   -= $horizontalScrollOffset;

				// Translate the values to canvas rows and columns.
				$dirtyStartRow    += $top;
				$dirtyStartColumn += $left;
				$dirtyEndRow      += $top;
				$dirtyEndColumn   += $left;

				// Expand the canvas dirty regions to include the new dirty regions from this TextBox
				if($dirtyStartRow > $this->height)
				{
					$canvasDirtyStartRow    = $this->height;
					$canvasDirtyStartColumn = $this->width;
				}
				elseif($dirtyStartRow < $canvasDirtyStartRow)
				{
					$canvasDirtyStartRow    = $dirtyStartRow;
					$canvasDirtyStartColumn = $dirtyStartColumn;
				}
				elseif($dirtyStartRow == $canvasDirtyStartRow && $dirtyStartColumn < $canvasDirtyStartColumn)
				{
					$canvasDirtyStartColumn = $dirtyStartColumn;
				}

				if($dirtyEndRow > $this->height)
				{
					$canvasDirtyEndRow    = $this->height;
					$canvasDirtyEndColumn = $this->width;
				}
				elseif($canvasDirtyEndRow < $dirtyEndRow)
				{
					$canvasDirtyEndRow    = $dirtyEndRow;
					$canvasDirtyEndColumn = $dirtyEndColumn;
				}
				elseif($canvasDirtyEndRow == $dirtyEndRow && $canvasDirtyEndColumn < $dirtyEndColumn)
				{
					$canvasDirtyEndColumn = $dirtyEndColumn;
				}
			}
		}

		$cursorHasMoved = false;

		// TODO sort elements in correct drawing order
		foreach($this->displayElements as $displayElement)
		{
			list($top, $left, $width, $height) = self::_getScreenDimensions($displayElement);

			// Check if the element falls within the dirty region.
			if($top + 1 <= $canvasDirtyEndRow || $canvasDirtyStartRow <= $top + $height)
			{
				// Draw the element to the canvas
				$this->buffer = Ype_Util::writeToTextRegion(
					$this->buffer,
					$displayElement->buffer,
					$top,
					$left,
					$width,
					$height,
					$displayElement->horizontalScrollOffset,
					$displayElement->verticalScrollOffset
				);
				$displayElement->dirtySections = array();
			}

			$cursorHasMoved |= $displayElement->cursorHasMoved;
			$displayElement->cursorHasMoved = false;
		}

		// TODO keep track of dirty buffer regions.
		$text = '';
		for($i = $canvasDirtyStartRow - 1; $i < $canvasDirtyEndRow; $i++)
		{
			if(isset($this->buffer[$i]))
			{
				$text .= Ype_AnsiEscapeSequence::getCsiEscapeSequence(Ype_AnsiEscapeSequence::CSI_CUP, $i + 1, 1) .
				         $this->buffer[$i] .
				         Ype_AnsiEscapeSequence::getCsiEscapeSequence(
					         Ype_AnsiEscapeSequence::CSI_EL,
					         Ype_AnsiEscapeSequence::EL_RIGHT
					     );
			}
		}

		if(null !== $this->activeDisplayElement)
		{
			list($top, $left, $width, $height) = self::_getScreenDimensions($this->activeDisplayElement);

			$newCursorRow = $this->activeDisplayElement->currentRow + $top;
			if($newCursorRow > $this->height)
			{
				$newCursorRow = $this->height;
			}

			$newCursorColumn = $this->activeDisplayElement->currentColumn + $left;
			if($newCursorColumn > $this->width)
			{
				$newCursorColumn = $this->width;
			}

			$suffix = Ype_AnsiEscapeSequence::getCsiEscapeSequence(
				Ype_AnsiEscapeSequence::CSI_CUP,
				$newCursorRow,
				$newCursorColumn
			);

			if('' != $text || $cursorHasMoved)
			{
				$text .= $suffix;
			}

			if('' != $text)
			{
				Ype::message('writeToStandardOutput', array($text));
			}
		}

//		Ype::message('writeEscapeSequenceToStandardOutput', array(Ype_AnsiEscapeSequence::CSI_WM_REPORT_TEXT_AREA_SIZE));
	}

	/**
	 * Translates the dimensions of the TextBox to absolute TextCanvas dimensions.
	 * @param Ype_Plugin_Screen_TextBox $textBox
	 * @return array
	 */
	private function _getScreenDimensions(Ype_Plugin_Screen_TextBox $textBox)
	{
		$rowCount = 1 + substr_count($textBox->buffer, "\n");

		$top      = $textBox->top;
		$left     = $textBox->left;
		$width    = (null === $textBox->width)  ? $this->width                  : $textBox->width;
		$height   = (null === $textBox->height) ? min($this->height, $rowCount) : $textBox->height;

		if(null === $top)
		{
			if(null === $textBox->bottom)
			{
				$top = 0;
			}
			else
			{
				$top = max(0, $this->height - $height - $textBox->bottom);
			}
		}
		else
		{
			if(null !== $textBox->bottom)
			{
				$height = max(0, $this->height - $top - $textBox->bottom);
			}
		}

		if(null === $left)
		{
			if(null === $textBox->right)
			{
				$left = 0;
			}
			else
			{
				$left = max(0, $this->width - $width - $textBox->right);
			}
		}
		else
		{
			if(null !== $textBox->right)
			{
				$width = max(0, $this->width - $left - $textBox->right);
			}
		}

		return array($top, $left, $width, $height);
	}

	public function onMoveTextBoxCursor($displayElementId = 0, $rowOffset = 0, $columnOffset = 0)
	{
		$displayElement = $this->displayElements[$displayElementId];
		$displayElement->moveCursor($rowOffset, $columnOffset);
		$this->draw();
	}

	public function onSetTextBoxCursorPosition($displayElementId = 0, $row = null, $column = null)
	{
		$displayElement = $this->displayElements[$displayElementId];
		if(null !== $row)
		{
			$displayElement->currentRow = $row;
		}

		if(null !== $column)
		{
			$displayElement->currentColumn = $column;
		}
	}

	public function onWriteToTextBox($displayElementId = 0, $data = '', $row = null, $column = null)
	{
		$this->displayElements[$displayElementId]->write($data, $row, $column, true);
		$this->draw();
	}

	public function onDeleteSelectionFromTextBox($displayElementId = 0, $endRow, $endColumn, $startRow = null, $startColumn = null)
	{
		$this->displayElements[$displayElementId]->deleteSelection($endRow, $endColumn, $startRow, $startColumn);
	}

	public function onDeleteCharsFromTextBox($displayElementId = 0, $charCount = 1, $startRow = null, $startColumn = null)
	{
		$this->displayElements[$displayElementId]->delete($charCount, $startRow, $startColumn);
		$this->draw();
	}

	public function onSetActiveTextBox($textBoxId)
	{
		if(isset($this->displayElements[$textBoxId]))
		{
			$this->activeDisplayElement = $this->displayElements[$textBoxId];
		}
	}

	public function onCreateTextBox(
		$name               = self::DEFAULT_DISPLAY_ELEMENT_NAME,
		$top                = null,
		$right              = null,
		$bottom             = null,
		$left               = null,
	    $width              = null,
	    $height             = null,
		$successMessageName = self::MESSAGE_NAME_TEXT_BOX_CREATE_SUCCESS,
		$data               = '',
		$makeActive         = false,
		$makeDefault        = false
	)
	{
		if(isset($this->displayElementIds[$name]))
		{
			throw new Exception("TextCanvas '{$name}' already exists.'");
		}
		$textBox = new Ype_Plugin_Screen_TextBox($this, $top, $right, $bottom, $left, $width, $height);
		if('' != $data)
		{
			$textBox->write($data);
		}

		$this->displayElements[$textBox->id] = $textBox;
		$this->displayElementIds[$name]      = $textBox->id;

		if($makeActive || null === $this->activeDisplayElement)
		{
			$this->activeDisplayElement = $textBox;
		}

		$currentSenderId = Ype::getCurrentSenderId();

		if($makeDefault)
		{
			$this->pluginDefaultDisplayElementIds[$currentSenderId] = $textBox->id;
		}

		$this->draw();

		Ype::message($successMessageName, array($textBox->id), $currentSenderId, Ype::getCurrentMessageToken());
		Ype::message(self::MESSAGE_NAME_TEXT_BOX_CREATED, array($textBox->id));
	}

	public function onSetDefaultTextBox($textBoxId)
	{
		$this->pluginDefaultDisplayElementIds[Ype::getCurrentSenderId()] = $textBoxId;
	}

	public function onProcessStandardOutput($line)
	{
		$currentSenderId = Ype::getCurrentSenderId();

		Ype_Log::debug(__CLASS__, "onProcessStandardOutput() currentSenderId: {$currentSenderId}");
		if(isset($this->pluginDefaultDisplayElementIds[$currentSenderId]))
		{
			$this->onWriteToTextBox(
				$this->pluginDefaultDisplayElementIds[$currentSenderId],
				$line
			);
		}
	}

	public function onProcessStandardError($line)
	{
		$currentSenderId = Ype::getCurrentSenderId();
		if(isset($this->pluginDefaultDisplayElementIds[$currentSenderId]))
		{
			$this->onWriteToTextBox(
				$this->pluginDefaultDisplayElementIds[$currentSenderId],
				$line
			);
		}
	}

}