<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2016 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype\Plugin
 * @author	  Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2016 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/**
 * @package Ype\Plugin
 */
class Ype_Plugin_Prompt extends Ype_AbstractProcessCompatiblePlugin
{
	const DEFAULT_PROMPT = "ype > ";

	protected $history = array();
	protected $textBoxCreationToken = null;
	protected $textBoxId = null;
	protected $left = '';
	protected $right = '';
	protected $prompt = self::DEFAULT_PROMPT;

	public function onInitializePlugin()
	{
		// Do nothing.
	}

	public function onPluginAvailable($pluginClass = null)
	{
		if('Ype_Plugin_TextCanvas' == $pluginClass && null === $this->textBoxCreationToken)
		{
			$this->textBoxCreationToken = Ype::message('createTextBox', array(
				'name'               => 'prompt',
				'successMessageName' => 'promptTextBoxCreated',
				'data'               => $this->prompt,
				'top'                => null,
				'right'              => 0,
				'bottom'             => 0,
				'left'               => 0,
				'height'             => null,
				'width'              => null,
				'makeActive'         => true,
				'makeDefault'        => false,
			));
		}
	}

	public function onPromptTextBoxCreated($windowId = 0)
	{
		$this->textBoxId = $windowId;
		$this->ready();
	}

	public function onNewCharacterCode($characterCode = '')
	{
		// Ignore character codes if the TextBox hasn't been created yet.
		if(null === $this->textBoxId)
		{
			Ype_Log::debugFunctionCall('IGNORED');

			return;
		}

		Ype_Log::debugFunctionCall();
		Ype_Log::debug('Plugin_Prompt', 'messageToken: ' . Ype::getBroker()->getCurrentMessageToken());
		Ype_Log::debug('Plugin_Prompt', 'senderId: ' . Ype::getBroker()->getCurrentContextPluginId());

		$currentLeft  = $this->left;
		$currentRight = $this->right;

		if($characterCode > 31 && $characterCode != Ype_ASCII::DEL)
		{
			Ype::message(
				'writeToTextBox',
				array(
					'displayElementId' => $this->textBoxId,
					'data'             => chr($characterCode),
				)
			);
			$currentLeft .= chr($characterCode);
		}
		else
		{
			switch($characterCode)
			{
				// 1: ^A - go to start of line
				case Ype_ASCII::SOH:
					// TODO Add "property count" to ASCII to do this properly
					Ype::message('moveTextBoxCursor', array(
						'displayElementId' => $this->textBoxId,
						'columnOffset'     => -(strlen($currentLeft)),
					));
					$currentRight = $currentLeft . $currentRight;
					$currentLeft  = '';
					break;

				// 4: ^D - Exit, if we're on an empty line.
				case Ype_ASCII::EOT:
					if(empty($currentLeft) && empty($currentRight))
					{
						Ype::message('quit');
					}
					break;

				// 5: ^E - go to end of line
				case Ype_ASCII::ENQ:
					Ype::message('moveTextBoxCursor', array(
						'displayElementId' => $this->textBoxId,
						'columnOffset'     => strlen($currentRight),
					));
					$currentLeft .= $currentRight;
					$currentRight = '';
					break;

				// 8:   ^H - Backspace
				case Ype_ASCII::BS:
					// 127: ^? - Backspace
				case Ype_ASCII::DEL:
					// TODO handle multi-line correctly
					// We got ourselves a backspace char
					if(!empty($currentLeft))
					{
						$currentLeft = substr($currentLeft, 0, -1);
						Ype::message('deleteCharsFromTextBox', array(
							'displayElementId' => $this->textBoxId,
							'charCount'        => -1,
						));
					}
					break;

				// 9: TAB character. Ignore for now.
				case Ype_ASCII::HT:
					break;

				// 10: Newline.
				case Ype_ASCII::LF:
				// 13: Carriage Return.
				case Ype_ASCII::CR:
					Ype::message('executePhpCode', array("{$currentLeft}{$currentRight}"));
					// Conveniently continue to the next case which clears the line.
				// 3: ^C - Abort current line.
				case Ype_ASCII::ETX:
					Ype::message('deleteCharsFromTextBox', array(
						'displayElementId' => $this->textBoxId,
						'charCount'        => -strlen($currentLeft),
					));
					$currentLeft = '';
					Ype::message('deleteCharsFromTextBox', array(
						'displayElementId' => $this->textBoxId,
						'charCount'        => strlen($currentRight),
					));
					$currentRight = '';
					break;
			}
		}

		if($this->left != $currentLeft || $this->right != $currentRight)
		{
			$this->left  = $currentLeft;
			$this->right = $currentRight;
		}
	}

	public function onNewEscapeSequence($escapeSequence = '')
	{
		// Ignore character codes if the TextBox hasn't been created yet.
		if(null === $this->textBoxId)
		{
			Ype_Log::debugFunctionCall('IGNORED');
			return;
		}


		$currentLeft  = $this->left;
		$currentRight = $this->right;
		$lineCache    = $this->history;

		$pattern = Ype_AnsiEscapeSequence::getPatternForSequence($escapeSequence);

		Ype_Log::debugFunctionCall($pattern);

		$amount = 1;
		switch($pattern)
		{
			// Cursor Up
			case Ype_AnsiEscapeSequence::CSI_CUU:
//				sscanf($escapeSequence, $pattern, $amount);
//				if($currentLineId >= $amount)
//				{
//					$currentLineId -= $amount;
//				}
				break;

			// Cursor Down
			case Ype_AnsiEscapeSequence::CSI_CUD:
//				sscanf($escapeSequence, $pattern, $amount);
//				if($currentLineId < count($this->history))
//				{
//					$currentLineId++;
//				}
				break;

			// Cursor Forward
			case Ype_AnsiEscapeSequence::SS3_CUF:
			case Ype_AnsiEscapeSequence::CSI_CUF:
				$amount = 1;
				sscanf($escapeSequence, $pattern, $amount);
				if(!empty($currentRight))
				{
					self::_moveLeftRight($currentLeft, $currentRight, $amount);
					Ype::message('moveTextBoxCursor', array(
						'displayElementId' => $this->textBoxId,
						'columnOffset'     => $amount
					));
				}
				break;

			// Cursor Backward
			case Ype_AnsiEscapeSequence::SS3_CUB:
			case Ype_AnsiEscapeSequence::CSI_CUB:
				$amount = 1;
				sscanf($escapeSequence, $pattern, $amount);
				if(!empty($currentLeft))
				{
					$amount = -$amount;
					self::_moveLeftRight($currentLeft, $currentRight, $amount);
					Ype::message('moveTextBoxCursor', array(
						'displayElementId' => $this->textBoxId,
						'columnOffset'     => $amount
					));
				}
				break;

			// Home
			case Ype_AnsiEscapeSequence::CSI_CUP_HOME:
			case Ype_AnsiEscapeSequence::CSI_HOME:
			case Ype_AnsiEscapeSequence::SS3_HOME:
				Ype::message('moveTextBoxCursor', array(
					'displayElementId' => $this->textBoxId,
					'columnOffset'     => -(strlen($currentLeft)),
				));
				$currentRight = $currentLeft . $currentRight;
				$currentLeft  = '';
				break;

			// End
			case Ype_AnsiEscapeSequence::CSI_CPL_END:
			case Ype_AnsiEscapeSequence::CSI_END:
			case Ype_AnsiEscapeSequence::SS3_END:
				Ype::message('moveTextBoxCursor', array(
					'displayElementId' => $this->textBoxId,
					'columnOffset'     => strlen($currentRight),
				));
				$currentLeft .= $currentRight;
				$currentRight = '';
				break;

			// Delete
			case Ype_AnsiEscapeSequence::CSI_DELETE:
				if(!empty($currentRight))
				{
					$currentRight = substr($currentRight, 1);
				}
				Ype::message('deleteCharsFromTextBox', array(
					'displayElementId' => $this->textBoxId,
					'charCount'        => 1,
				));
				break;

			// TODO Option + Left on Mac OS, should go one word bak.
			case Ype_ASCII::WORD_START;
				break;

			// TODO Option + Right on Mac OS, should go one word forward.
			case Ype_ASCII::WORD_END;
				break;
		}

//		if($this->lineId != $currentLineId)
//		{
//			if(!isset($lineCache[$this->lineId]))
//			{
//				$lineCache[$lineId] = self::getLine($lineId);
//			}
//
//			$left  = $lineCache[$lineId];
//			$right = '';
//			$diff  = strlen($left) - strlen($currentLeft);
//			Ype_ASCII::cursorForward($diff);
//			self::_updateLeft($currentLeft, $left);
//		}

		if($this->left != $currentLeft || $this->right != $currentRight)
		{
			$this->left  = $currentLeft;
			$this->right = $currentRight;
		}
	}

	public function onUpdatedLineBuffers($leftBuffer, $rightBuffer)
	{
		// TODO: Implement onUpdatedLineBuffers() method.
	}


	protected static function _moveLeftRight(&$left, &$right, &$amount)
	{
		if($amount > 0)
		{
			$rightLength = strlen($right);
			if($rightLength < $amount)
			{
				$amount = $rightLength;
			}

			if($amount > 0)
			{
				$left .= substr($right, 0, $amount);
				$right = substr($right, $amount);
			}
		}
		elseif($amount < 0)
		{
			$leftLength = strlen($left);
			if($amount < -$leftLength)
			{
				$amount = -$leftLength;
			}

			$right = substr($left, $amount) . $right;
			$left  = substr($left, 0, $amount);
		}
	}
}