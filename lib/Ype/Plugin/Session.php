<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2015 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype\Plugin
 * @author	  Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2015 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/**
 * @package Ype\Plugin
 */
class Ype_Plugin_Session extends Ype_AbstractProcessCompatiblePlugin
{
	protected $textBoxCreationToken = null;
	protected $textBoxId = null;

	private $_buffer = '';

	public function onPluginAvailable($pluginClass = '')
	{
		if('Ype_Plugin_TextCanvas' == $pluginClass && null === $this->textBoxCreationToken)
		{
			$this->textBoxCreationToken = Ype::message('createTextBox', array(
				'name'               => 'session',
				'successMessageName' => 'sessionTextBoxCreated',
				'top'                => 0,
				'right'              => 0,
				'bottom'             => 1,
				'left'               => 0,
				'height'             => null,
				'width'              => null,
				'makeDefault'        => true,
			));
		}
	}

	public function onSessionTextBoxCreated($textBoxId)
	{
		$this->textBoxId = $textBoxId;
		$this->ready();
	}

	public function onExecutePhpCode($phpCode)
	{
		Ype_Log::debugFunctionCall();

		$phpCode = trim($phpCode);
		if($phpCode[strlen($phpCode) - 1] != ';')
		{
			$phpCode .= ';';
		}

		if($phpCode[0] == '=')
		{
			$phpCode = '$__ypeEvalResult = ' . substr($phpCode, 1);
		}

		$lintLine = '<?php ' . $phpCode . ' ?>';
		$lintFile = 'lint.' . getmypid() . '.php';
		Ype::writeToConfigFile($lintFile, $lintLine);
		$lintPath  = Ype::ensureConfigPath($lintFile);
		$output    = array();
		$returnVar = 1337;

		$result = exec(PHP_EXECUTABLE . " -l {$lintPath}", $output, $returnVar);
		unlink($lintPath);

		if(0 != $returnVar)
		{
			// output error
			fwrite(STDERR, $result);
		}
		else
		{
			$__ypeEvalResult = null;

			// Attempt to catch any Exceptions thrown
			try
			{
				eval($phpCode);
			}
			catch(Exception $e)
			{
				fwrite(STDERR, $e->getMessage());
			}

			if(null !== $__ypeEvalResult)
			{
				Ype::message('writeToTextBox', array(
					'displayElementId' => $this->textBoxId,
					'data'             => $__ypeEvalResult . PHP_EOL,
				));
			}
		}
	}

	protected function onNewEscapeSequence($escapeSequence)
	{
		// TODO: Implement onNewEscapeSequence() method.
	}

	protected function onUpdatedLineBuffers($leftBuffer, $rightBuffer)
	{
		$this->_buffer .= $leftBuffer . $rightBuffer;
		// TODO: Implement onUpdatedLineBuffers() method.
	}
}