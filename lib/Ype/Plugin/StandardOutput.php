<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2015 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype\Plugin
 * @author	  Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2015 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/**
 * @package Ype\Plugin
 */
class Ype_Plugin_StandardOutput extends Ype_AbstractPlugin
{
	/** @var Ype_NonBlockingStream_Handler */
	protected $streamHandler;
	
	/** @var string */
	protected $outgoingData = '';

	/**
	 * @param Ype_NonBlockingStream_Handler $streamHandler
	 */
	public function __construct(Ype_NonBlockingStream_Handler $streamHandler)
	{
		$this->streamHandler = $streamHandler;
	}

	public function onQuit($message = '')
	{
		$this->streamHandler->unregisterWriteStream(STDOUT);
		
		parent::onQuit($message);
	}

	public function onWriteToStandardOutput($data)
	{
		if(strlen($this->outgoingData) == 0)
		{
			$this->streamHandler->registerWriteStream(STDOUT, array($this, 'writeCallback'));
		}

		$this->outgoingData .= $data;
	}

	/**
	 * @param string $sequence
	 */
	public function onWriteEscapeSequenceToStandardOutput($sequence)
	{
		$this->onWriteToStandardOutput(chr(Ype_ASCII::ESC) . $sequence);
	}

	/**
	 * @param resource $stream
	 */
	public function writeCallback($stream)
	{
		if(strlen($this->outgoingData) > 0)
		{
			$numWritten         = fwrite($stream, $this->outgoingData);
			$this->outgoingData = substr($this->outgoingData, $numWritten);
		}

		if(strlen($this->outgoingData) == 0)
		{
			$this->streamHandler->unregisterWriteStream(STDOUT);
		}
	}
}