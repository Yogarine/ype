<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2016 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype\Plugin\Screen
 * @author    Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2016 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/**
 * @package Ype\Plugin\Screen
 */
class Ype_Plugin_Screen_TextBox extends Ype_Plugin_Screen_DisplayElement
{
	/** @var string Contents of the TextBox. */
	public $buffer = '';

	/** @var array Collection of arrays containing start and end positions of dirty sections in the buffer.
	 * @example array(
	 *              $dirtyStartRow,
	 *              $dirtyStartColumn,
	 *              $dirtyEndRow,
	 *              $dirtyEndColumn,
	 *          )*/
	public $dirtySections = array();

	/** @var bool Keeps track of whether the cursor has moved. */
	public $cursorHasMoved = false;

	/** @var int Current vertical scroll position. */
	public $verticalScrollOffset = 0;

	/** @var int Current horizontal scroll position. */
	public $horizontalScrollOffset = 0;

	/** @var int Current cursor row */
	public $currentRow = 1;

	/** @var int Current cursor column */
	public $currentColumn = 1;

	public function moveCursor($rowOffset, $columnOffset)
	{
		$this->cursorHasMoved = true;

		$this->dirtySections[] = array(
			$this->currentRow,
			$this->currentColumn,
			$this->currentRow + $rowOffset,
			$this->currentColumn + $columnOffset,
		);

		$this->currentRow    += $rowOffset;
		$this->currentColumn += $columnOffset;
	}

	public function write($data, $row = null, $column = null, $insert = true)
	{
		if($row !== null)
		{
			$this->currentRow = $row;
		}

		if($column !== null)
		{
			$this->currentColumn = $column;
		}

		Ype_Log::debug(__CLASS__, "currentRow: {$this->currentRow}");
		Ype_Log::debug(__CLASS__, "currentColumn: {$this->currentColumn}");

		if($data != '')
		{
			$this->cursorHasMoved = true;

			$dirtyStartRow    = $this->currentRow;
			$dirtyStartColumn = $this->currentColumn;
			$this->buffer     = Ype_Util::writeText($this->buffer, $data, $this->currentRow, $this->currentColumn,
			                                        $insert);

			$this->dirtySections[] = array(
				$dirtyStartRow,
				$dirtyStartColumn,
				$this->currentRow,
				$this->currentColumn,
			);

			Ype_Log::debug('dirty sections', $this->dirtySections);
		}
	}

	public function deleteSelection($endRow, $endColumn, $startRow = null, $startColumn = null)
	{
		if($startRow !== null)
		{
			$this->currentRow = $startRow;
		}

		if($startColumn !== null)
		{
			$this->currentColumn = $startColumn;
		}

		if($startRow < $endRow || ($startRow == $endRow && $startColumn < $endColumn))
		{
			$this->cursorHasMoved = true;

			$this->buffer  = Ype_Util::deleteText(
				$this->buffer,
				$this->currentRow,
				$this->currentColumn,
				$endRow,
				$endColumn
			);

			$this->dirtySections[] = array(
				$this->currentRow,
				$this->currentColumn,
				$endRow,
				$endColumn,
			);
		}
	}

	/**
	 * Delete $count amount of characters.
	 * @todo implement proper deleting across newlines.
	 * @param integer $count       Amount of characters to delete.
	 *                             If positive the characters will be delete from the right of $startColumn.
	 *                             If negative then it will be backspaced from the left.
	 * @param integer $startRow
	 * @param integer $startColumn
	 */
	public function delete($count, $startRow = null, $startColumn = null)
	{
		if($startRow !== null)
		{
			$this->currentRow = $startRow;
		}

		if($startColumn !== null)
		{
			$this->currentColumn = $startColumn;
		}

		$this->cursorHasMoved = true;

		$endRow    = $this->currentRow;
		$endColumn = $this->currentColumn + $count;
		$this->buffer = Ype_Util::deleteText(
			$this->buffer,
			$this->currentRow,
			$this->currentColumn,
			$endRow,
			$endColumn
		);

		$this->dirtySections[] = array(
			$this->currentRow,
			$this->currentColumn,
			$endRow,
			$endColumn
		);
	}
}