<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2015 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype
 * @author	  Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2015 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/**
 * @package Ype
 */
class Ype_Session
{
	/** @var Ype_NonBlockingStream_Process */
	protected $_process = null;
	/** @var string */
	protected $_bootstrap = '';
	/** @var int Counter  */
	protected static $_executeCount = 0;

	/**
	 * @param  string  $bootstrap  OPTIONAL
	 */
	public function __construct($bootstrap = '')
	{
		$this->_bootstrap = $bootstrap;
	}

	/**
	 * @return Ype_NonBlockingStream_Process
	 */
	public function getProcess()
	{
		if($this->_process === null)
		{
			$this->_process = new Ype_NonBlockingStream_Process();
			$this->_initBootstrap();
		}
		elseif(!$this->_process->isRunning())
		{
			Ype_Log::warning('Ype', 'Session died, starting new...');
			$this->_process = null;
			$this->_process = new Ype_NonBlockingStream_Process();
			$this->_initBootstrap();
		}

		return $this->_process;
	}

	private function _initBootstrap()
	{
		global $argv;

		Ype_Log::info('Ype_Session', "Initializing bootstrap '{$this->_bootstrap}'");

		if($this->_bootstrap != '')
		{
			if(file_exists($this->_bootstrap))
			{
				$jsonArgv = json_encode($argv);
				$this->_process->write("include '{$this->_bootstrap}';" . PHP_EOL);
				$this->_process->write("if(isset(\$bootstrap) && \$bootstrap instanceof Ype_Bootstrap) " .
									   "\$bootstrap->bootstrap('{$jsonArgv}');" . PHP_EOL);
			}
			else
			{
				Log::warning('Session', "Bootstrap '{$this->_bootstrap}' not found!");
			}
		}
	}

	public function getIncludePaths()
	{
		$errors = array();

		$includePath  = $this->execute('get_include_path()', $errors, true);
		$includePaths = explode(PATH_SEPARATOR, $includePath);

		$realIncludePaths = array();
		foreach($includePaths as $path)
		{
			$realIncludePaths[] = realpath($path);
		}

		return $realIncludePaths;
	}

	/**
	 * @param  string    $line
	 * @param  string[]  $errors
	 * @param  boolean   $getResult
	 * @return int
	 */
	public function execute($line, array &$errors = array(), $getResult = false)
	{
		self::$_executeCount++;
		$result = '';

		$line = trim($line);
		$line = str_replace("$", "\$", $line);
		if(strrpos($line, ";") !== strlen($line) - 1)
		{
			$line .= ";";
		}

		if($getResult)
		{
			$line = "\$__ypeResult = {$line}";
		}

		$id = uniqid(self::$_executeCount, true);

		$process = $this->getProcess();
		$process->write($line . PHP_EOL);

		if($getResult)
		{
			$process->write("echo '__YPE_{$id}_RESULT,' . base64_encode(print_r(\$__ypeResult, true)) . PHP_EOL;" . PHP_EOL);
		}

		$process->write("echo '__YPE_{$id}_END' . PHP_EOL;" . PHP_EOL);
		$process->flush();

		Ype_Log::debug('Session', 'looping until we get YPE result output...');
		$count = 0;
		$stop = false;
		do
		{
			$output = $process->selectAndReadStdout(1);

			if($output === false)
			{
				if($count >= 3)
				{
					Ype_Log::error('Session', "Didn't get END response from Process!");
					break;
				}
				$count++;
				Ype_Log::warning('Session', "Select failed prematurely.");
			}
			else
			{
				$matches  = array();
				$quotedId = preg_quote($id);

				if(preg_match("/^(.*)__YPE_{$quotedId}_(.*)$/", $output, $matches))
				{
					$output  = $matches[1];
					$data    = explode(',', $matches[2]);

					switch($data[0])
					{
						case 'RESULT':
							$result .= base64_decode($data[1]);
							break;
						case 'END':
							$stop = true;
							break;
					}

					echo $output;
				}
				else
				{
					echo $output . PHP_EOL;
				}
			}
		}
		while($process->isRunning() && $stop == false);

		do
		{
			$error = $process->selectAndReadStderr(0);
			$errors[] = $error;
		}
		while($error !== '' && $error !== false);

		return $result;
	}
}