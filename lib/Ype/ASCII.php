<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2015 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype
 * @author	  Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2015 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/**
 * @package Ype
 */
abstract class Ype_ASCII
{
	// C0 Control Codes
	/**
	 * Null.
	 * Ctrl-@
	 * Originally used to allow gaps to be left on paper tape for edits. Later used for padding after a code that might take a
	 * terminal some time to process (e.g. a carriage return or line feed on a printing terminal). Now often used as a string
	 * terminator, especially in the C programming language.
	 */
	const NUL = 0;
	/**
	 * Start of Header.
	 * Ctrl-A
	 * First character of a message header.
	 */
	const SOH = 1;
	/**
	 * Start of Text.
	 * Ctrl-B
	 * First character of message text, and may be used to terminate the message heading.
	 */
	const STX = 2;
	/**
	 * End of Text.
	 * Ctrl-C
	 * Often used as a "break" character (Ctrl+C) to interrupt or terminate a program or process.
	 */
	const ETX = 3;
	/**
	 * End of Transmission.
	 * Ctrl-D
	 * Used on Unix to signal end-of-file condition on, or to logout from a terminal.
	 */
	const EOT = 4;
	/**
	 * Enquiry.
	 * Ctrl-E
	 * Signal intended to trigger a response at the receiving end, to see if it is still present.
	 */
	const ENQ = 5;
	/**
	 * Acknowledge.
	 * Ctrl-F
	 * Response to an ENQ, or an indication of successful receipt of a message.
	 */
	const ACK = 6;
	/**
	 * Bell.
	 * Ctrl-G
	 * Originally used to sound a bell on the terminal. Later used for a beep on systems that didn't have a physical bell. May
	 * also quickly turn on and off inverse video (a visual bell).
	 */
	const BEL = 7;
	/**
	 * BackSpace.
	 * Ctrl-H
	 * Move the cursor one position leftwards. On input, this may delete the character to the left of the cursor. On output,
	 * where in early computer technology a character once printed could not be erased, the backspace was sometimes used to
	 * generate accented characters in ASCII. For example, à could be produced using the three character sequence a BS ` (0x61
	 * 0x08 0x60). This usage is now deprecated and generally not supported. To provide disambiguation between the two potential
	 * uses of backspace, the cancel character control code was made part of the standard C1 control set.
	 */
	const BS = 8;
	/**
	 * Horizontal Tabulation.
	 * Ctrl-I
	 * Position to the next character tab stop.
	 */
	const HT = 9;
	/**
	 * Line Feed.
	 * Ctrl-J
	 * On typewriters, printers, and some terminal emulators, moves the cursor down one row without affecting its column position.
	 * On Unix, used to mark end-of-line. In MS-DOS, Windows, and various network standards, LF is used following CR as part of the
	 * end-of-line mark.
	 */
	const LF = 10;
	/**
	 * Vertical Tabulation.
	 * Ctrl-K
	 * Position the form at the next line tab stop.
	 */
	const VT = 11;
	/**
	 * Form Feed.
	 * Ctrl-L
	 * On printers, load the next page. Treated as whitespace in many programming languages, and may be used to separate logical
	 * divisions in code. In some terminal emulators, it clears the screen.
	 */
	const FF = 12;
	/**
	 * Carriage Return.
	 * Ctrl-M
	 * Originally used to move the cursor to column zero while staying on the same line. On Mac OS (pre-Mac OS X), as well as in
	 * earlier systems such as the Apple II and Commodore 64, used to mark end-of-line. In MS-DOS, Windows, and various network
	 * standards, it is used preceding LF as part of the end-of-line mark. The Enter or Return key on a keyboard will send this
	 * character, but it may be converted to a different end-of-line sequence by a terminal program.
	 */
	const CR = 13;
	/**
	 * Shift Out.
	 * Ctrl-N
	 * Switch to an alternate character set.
	 */
	const SO = 14;
	/**
	 * Shift In.
	 * Ctrl-O
	 * Return to regular character set after Shift Out.
	 */
	const SI = 15;
	/**
	 * Data Link Escape.
	 * Ctrl-P
	 * Cause the following octets to be interpreted as raw data, not as control codes or graphic characters. Returning to normal
	 * usage would be implementation dependent.
	 */
	const DLE = 16;
	/**
	 * Device Control 1.
	 * Ctrl-Q
	 * Control code reserved for device control, with the interpretation dependent upon the device they were connected.
	 * DC1 was intended primarily to indicate activating a device. In actual practice DC1 and DC3 (known also as XON and XOFF
	 * respectively in this usage) quickly became the de facto standard for software flow control.
	 */
	const DC1 = 17;
	/**
	 * Device Control 2.
	 * Ctrl-R
	 * Control code reserved for device control, with the interpretation dependent upon the device they were connected.
	 * DC2 was intended primarily to indicate activating a device.
	 */
	const DC2 = 18;
	/**
	 * Device Control 3.
	 * Ctrl-S
	 * Control code are reserved for device control, with the interpretation dependent upon the device they were connected.
	 * DC3 was intended primarily to indicate pausing a device. In actual practice DC1 and DC3 (known also as XON and XOFF
	 * respectively in this usage) quickly became the de facto standard for software flow control.
	 */
	const DC3 = 19;
	/**
	 * Device Control 4.
	 * Ctrl-T
	 * Control code reserved for device control, with the interpretation dependent upon the device they were connected.
	 * DC4 was intended primarily to indicate turning off a device.
	 */
	const DC4 = 20;
	/**
	 * Negative Acknowledge.
	 * Ctrl-U
	 * Sent by a station as a negative response to the station with which the connection has been set up.
	 * In binary synchronous communication protocol, the NAK is used to indicate that an error was detected in the previously
	 * received block and that the receiver is ready to accept retransmission of that block.
	 * In multipoint systems, the NAK is used as the not-ready reply to a poll.
	 */
	const NAK = 21;
	/**
	 * Synchronous Idle.
	 * Ctrl-V
	 * Used in synchronous transmission systems to provide a signal from which synchronous correction may be achieved between
	 * data terminal equipment, particularly when no other character is being transmitted.
	 */
	const SYN = 22;
	/**
	 * End of Transmission Block.
	 * Ctrl-W
	 * Indicates the end of a transmission block of data when data are divided into such blocks for transmission purposes.
	 */
	const ETB = 23;
	/**
	 * Cancel.
	 * Ctrl-X
	 * Indicates that the data preceding it are in error or are to be disregarded.
	 */
	const CAN = 24;
	/**
	 * End of Medium.
	 * Ctrl-Y
	 * Intended as means of indicating on paper or magnetic tapes that the end of the usable portion of the tape had been reached.
	 */
	const EM = 25;
	/**
	 * Substitute.
	 * Ctrl-Z
	 * Originally intended for use as a transmission control character to indicate that garbled or invalid characters had been
	 * received. It has often been put to use for other purposes when the in-band signaling of errors it provides is unneeded,
	 * especially where robust methods of error detection and correction are used, or where errors are expected to be rare enough
	 * to make using the character for other purposes advisable.
	 * In Ype we're going to use this for undo. :-)
	 */
	const SUB = 26;
	/**
	 * Escape.
	 * Ctrl-[
	 * The Esc key on the keyboard will cause this character to be sent on most systems. It can be used in software user
	 * interfaces to exit from a screen, menu, or mode, or in device-control protocols (e.g., printers and terminals) to signal that
	 * what follows is a special command sequence rather than normal text. In systems based on ISO/IEC 2022, even if another set of
	 * C0 control codes are used, this octet is required to always represent the escape character.
	 */
	const ESC = 27;
	/**
	 * File Separator.
	 * Ctrl-\
	 * Can be used as delimiters to mark fields of data structures. If used for hierarchical levels, US is the lowest level
	 * (dividing plain-text data items), while RS, GS, and FS are of increasing level to divide groups made up of items of the level
	 * beneath it.
	 */
	const FS = 28;
	/**
	 * Group Separator.
	 * Ctrl-]
	 * Can be used as delimiters to mark fields of data structures. If used for hierarchical levels, US is the lowest level
	 * (dividing plain-text data items), while RS, GS, and FS are of increasing level to divide groups made up of items of the level
	 * beneath it.
	 */
	const GS = 29;
	/**
	 * Record Separator.
	 * Ctrl-^
	 * Can be used as delimiters to mark fields of data structures. If used for hierarchical levels, US is the lowest level
	 * (dividing plain-text data items), while RS, GS, and FS are of increasing level to divide groups made up of items of the level
	 * beneath it.
	 */
	const RS = 30;
	/**
	 * Unit Separator.
	 * Ctrl-]
	 * Can be used as delimiters to mark fields of data structures. If used for hierarchical levels, US is the lowest level
	 * (dividing plain-text data items), while RS, GS, and FS are of increasing level to divide groups made up of items of the level
	 * beneath it.
	 */
	const US = 31;
	/**
	 * Space.
	 * Space is a graphic character. It has a visual representation consisting of the absence of a graphic symbol. It causes the
	 * active position to be advanced by one character position. In some applications, Space can be considered a lowest-level "word
	 * separator" to be used with the adjacent separator characters.
	 */
	const SP = 32;
	/**
	 * Delete.
	 * Ctrl-?
	 * Not technically part of the C0 control character range, this was originally used to mark deleted characters on paper tape,
	 * since any character could be changed to all ones by punching holes everywhere. On VT100 compatible terminals, this is the
	 * character generated by the key labelled ⌫, usually called backspace on modern machines, and does not correspond to the PC
	 * delete key.
	 */
	const DEL = 127;

	// C1 Control Characters
	/** Padding Character */
	const PAD = '@';
	/** High Octet Preset */
	const HOP = 'A';
	/** Break Permitted Here */
	const BPH = 'B';
	/** No Break Here */
	const NBH = 'C';
	/** Index */
	const IND = 'D';
	/** Next Line */
	const NEL = 'E';
	/** Start of Selected Area */
	const SSA = 'F';
	/** End of Selected Area */
	const ESA = 'G';
	/** Horizontal Tabulation Set */
	const HTS = 'H';
	/** Horizontal Tabulation With Justification */
	const HTJ = 'I';
	/** Vertical Tabulation Set */
	const VTS = 'J';
	/** Partial Line Down */
	const PLD = 'K';
	/** Partial Line Up */
	const PLU = 'L';
	/** Reverse Index */
	const RI  = 'M';
	/** Single Shift Select of G2 Character Set */
	const SS2 = 'N';
	/** Single Shift Select of G3 Character Set */
	const SS3 = 'O';
	/** Device Control String */
	const DCS = 'P';
	/** Private Use 1 */
	const PU1 = 'Q';
	/** Private Use 2 */
	const PU2 = 'R';
	/** Set Transmit State */
	const STS = 'S';
	/** Cancel Character */
	const CCH = 'T';
	/** Message Waiting */
	const MW  = 'U';
	/** Start of Protected Area */
	const SPA = 'V';
	/** End of Protected Area */
	const EPA = 'W';
	/** Start Of String */
	const SOS = 'X';
	/** Single Graphic Character Introducer */
	const SGCI = 'Y';
	/** Return Terminal ID. Obsolete form of CSI c (DA). */
	const DECID = 'Z';
	/** Control Sequence Introducer */
	const CSI = '[';
	/** String Terminator */
	const ST  = '\\';
	/** Operating System Command */
	const OSC = ']';
	/** Privacy Message */
	const PM  = '^';
	/** Application Program Command */
	const APC = '_';

	// No idea where these belong:
	/** Goes to the start of the current word. */
	const WORD_START = 'b';
	/** Goes to the end of the current word. */
	const WORD_END   = 'f';

	/** @var string[] List of escape characters */
	public static $ESCAPE_SEQUENCE_CHARACTERS = array(self::CSI, self::SS2, self::SS3);
}