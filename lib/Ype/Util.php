<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2015 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype
 * @author	  Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2015 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/**
 * @package Ype
 */
class Ype_Util
{
	/**
	 * @param $haystack
	 * @param $needle
	 * @param $limit
	 * @param int $offset
	 * @return bool|int
	 */
	public static function getNthStringPosition($haystack, $needle, $limit, $offset = 0)
	{
		$needleLength = strlen($needle);
		$position     = false;

		for($i = 0; $i < $limit; $i++)
		{
			$offset = strpos($haystack, $needle, $offset);
			if(false === $offset || null === $offset) {
				return $position;
			}

			$position = $offset;
			$offset  += $needleLength;
		}

		return $position;
	}

	public static function writeToTextRegion(
		array $textData,
		$replacementText,
		$top              = null,
		$left             = null,
		$width            = null,
		$height           = null,
		$horizontalOffset = 0,
		$verticalOffset   = 0
	)
	{
		$textRowCount = count($textData);
		if($textRowCount < $top + $height)
		{
			for($i = $textRowCount; $i < $top + $height; $i++)
			{
				$textData[$i] = '';
			}
		}
		$replacementData = explode("\n",  $replacementText);

		$replacementLineIndex = $verticalOffset;
		foreach($textData as $textLineIndex => &$textRow)
		{
			if(!isset($replacementData[$replacementLineIndex]))
			{
				// We're out of replacement data, so we're done here.
				break;
			}

			// Only start processing once we're past $row lines.
			if($top <= $textLineIndex && $textLineIndex < $top + $height)
			{
				$string = '';
				$textRowLength = strlen($textRow);

				// Get the first piece of the row if $column > 0.
				if($left > 0)
				{
					if($textRowLength >= $left)
					{
						// Text row is long enough, so grab the first part.
						$string .= substr($textRow, 0, $left);
					}
					else
					{
						// Text row is not long enough, grap it and suffix it with whitespace.
						$string .= str_pad($textRow, $left, ' ', STR_PAD_RIGHT);
					}
				}

				$replacementLine = $replacementData[$replacementLineIndex];
				$replacementLine = substr($replacementLine, $horizontalOffset);

				// Now check if we need to trim the string.
				$lineWidth = strlen($replacementLine);
				if(null !== $width && $width < $lineWidth)
				{
					$string .= substr($replacementLine, 0, $width - $left);
				}
//				elseif(null !== $width && $lineWidth < $width)
//				{
//					$string .= $replacementLine . str_repeat(' ', $width - $lineWidth);
//				}
				else
				{
					$string .= $replacementLine;
				}

				// TODO append leftover if replacement width < text width

				$textRow = $string;
				$replacementLineIndex++;
			}
		}

		return $textData;
	}

	/**
	 * @param string  $text               String to delete the text from.
	 * @param integer $startRow           Start row, starting at 1.
	 * @param integer $startColumn        Start column, starting at 1.
	 * @param integer $endRow             End row. Overwritten with the dirty end row.
	 * @param integer $endColumn          End column. Overwritten with the dirty end column.
	 * @return string
	 */
	public static function deleteText($text, &$startRow, &$startColumn, &$endRow, &$endColumn)
	{
		if(($endRow == $startRow && $endColumn < $startColumn) || $endRow < $startRow)
		{
			$oldEndRow = $endRow;
			$endRow    = $startRow;
			$startRow  = $oldEndRow;

			$oldEndColumn = $endColumn;
			$endColumn    = $startColumn;
			$startColumn  = $oldEndColumn;
		}

		$removeRowCount = $endRow - $startRow;
		$bufferRowCount = 1 + substr_count($text, "\n");
		$bufferLength   = strlen($text);

		// Get start position.
		if(1 == $startRow)
		{
			$startPosition = $startColumn - 1;
		}
		else
		{
			// We're not starting at the first row, so adjust the startPosition.

			// If the buffer contains less rows that what we're starting at, no need to do anything.
			if($bufferRowCount < $startRow)
			{
				return $text;
			}

			// Set the startPosition to the first character after $row new lines.
			$startPosition = $startColumn + Ype_Util::getNthStringPosition($text, "\n", $startRow - 1);
		}

		// Speed hack.
		if($startPosition > $bufferLength)
		{
			// We're starting writing _after_ the buffer ends, so no need to do anything.
			return $text;
		}

		// Now get the end position.
		if(0 == $removeRowCount)
		{
			$endPosition = $startPosition + ($endColumn - $startColumn);
		}
		else
		{
			$endPosition = Ype_Util::getNthStringPosition($text, "\n", $removeRowCount, $startPosition);

			// Get the length of the buffer row which the last data row is going to overwrite.
			$bufferRowLength = strpos($text, "\n", $endPosition);
			$bufferRowLength = (false === $bufferRowLength) ? $bufferLength - $endPosition + 1 : $bufferRowLength;

			// Increase the endPosition with the smallest of the two.
			$endPosition += min($endColumn - 1, $bufferRowLength);
		}

		if(0 == $startPosition)
		{
			// We're starting at the beginning.
			$prefix = '';
		}
		else
		{
			$prefix = substr($text, 0, $startPosition);
		}

		if($endPosition >= $bufferLength)
		{
			// If we're ending after the endPosition, just return the prefix.
			return $prefix;
		}

		$suffix = substr($text, $endPosition, $bufferLength);

		$endRow    = $bufferRowCount;

		return $prefix . $suffix;
	}

	/**
	 * @param           $text
	 * @param           $data
	 * @param int       $row    Row, starting at 1.
	 * @param int       $column Column, starting at 1
	 * @param bool|true $insert
	 * @return string
	 */
	public static function writeText($text, $data, &$row = 1, &$column = 1, $insert = true)
	{
		$dataRowCount   = 1 + substr_count($data, "\n");
		$bufferRowCount = 1 + substr_count($text, "\n");

		if(1 == $row)
		{
			$startPosition = 0;
		}
		else
		{
			// We're not starting at the first row, so adjust the startPosition.
			// If the buffer contains less rows that what we're starting at, add some newlines to make it match.
			if($bufferRowCount < $row)
			{
				$text .= str_repeat("\n", $row - $bufferRowCount);
			}

			// Set the startPosition to the first character after $row new lines.
			$startPosition = 1 + Ype_Util::getNthStringPosition($text, "\n", $row - 1);
		}

		// Increase startPosition with $column.
		$startPosition += $column - 1;

		$bufferLength = strlen($text);

		if(0 == $startPosition)
		{
			// We're starting at the beginning.
			$prefix = '';
		}
		elseif($startPosition > $bufferLength)
		{
			// We're starting writing _after_ the buffer ends, so the prefix is the current buffer incremented with whitespace.
			$prefix = $text . str_repeat(' ', $startPosition - $bufferLength);
		}
		else
		{
			$prefix = substr($text, 0, $startPosition);
		}

		// Grab the length of the last data row.
		$start = strrpos($data, "\n");
		if(false === $start)
		{
			$lastDataRowLength = strlen($data);

			// No newline, so we're incrementing the column
			$column += $lastDataRowLength;
		}
		else
		{
			$lastDataRowLength = strlen(substr($data, 1 + $start));

			// This is the actual last line length, so just set $column to that + 1.
			$column = 1 + $lastDataRowLength;
		}

		if($insert)
		{
			// Just append the rest.
			$suffix = substr($text, $startPosition);
		}
		elseif($bufferRowCount < $dataRowCount + $row)
		{
			// The number of rows in the buffer is smaller than the number of rows we're writing, so no suffix.
			$suffix = '';
		}
		else
		{
			// The number of rows in the buffer is equal or larger than what we're writing,
			// so first we set the end position to the start of the last row.
			$endPosition = Ype_Util::getNthStringPosition($text, "\n", $dataRowCount - 1, $startPosition);

			// Get the length of the buffer row which the last data row is going to overwrite.
			$bufferRowLength = strpos($text, "\n", $endPosition);
			$bufferRowLength = (false === $bufferRowLength) ? $bufferLength - $endPosition + 1 : $bufferRowLength;

			// Increase the endPosition with the smallest of the two.
			$endPosition += min($lastDataRowLength, $bufferRowLength);

			if($endPosition < $bufferLength)
			{
				$suffix = substr($text, $endPosition, $bufferLength - $endPosition + 1);
			}
			else
			{
				$suffix = '';
			}
		}

		if(false === $suffix)
		{
			$suffix = '';
		}

		$row += $dataRowCount - 1;

		return $prefix . $data . $suffix;
	}
} 