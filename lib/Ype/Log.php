<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2015 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype
 * @author	  Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2015 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/** Ensure DEBUG_BACKTRACE_PROVIDE_OBJECT is defined on pre-5.3.6 */
if(!defined('DEBUG_BACKTRACE_PROVIDE_OBJECT'))
{
	/** debug_backtrace() option. Populate the "object" index. */
	define('DEBUG_BACKTRACE_PROVIDE_OBJECT', 1);
}

/**
 * @package Ype
 */
abstract class Ype_Log
{
	const NONE    = 0;
	const ERROR   = 1;
	const WARNING = 2;
	const INFO    = 3;
	const DEBUG   = 4;
	const VERBOSE = 5;

	const FORMAT_DEFAULT = '[%t] %m';

	/** @var string Log format */
	protected static $_format = self::FORMAT_DEFAULT;

	/** @var int Log level */
	protected static $_logLevel = self::INFO;

	/** @var string[] Log levels */
	protected static $_logLevels = array(
		0 => 'NONE',
		1 => 'ERROR',
		2 => 'WARNING',
		3 => 'INFO',
		4 => 'DEBUG',
		5 => 'VERBOSE',
	);

	/** @var string[int] array */
	protected static $_logLevelColors = array(
		self::NONE    => '',
		self::ERROR   => 'Red',
		self::WARNING => 'Yellow',
		self::INFO    => 'Light Gray',
		self::DEBUG   => 'Gray',
		self::VERBOSE => 'Dark Gray',
	);

	/** @var boolean True if the following row should start with a newline */
	protected static $_appendable = false;

	/** @var string Last tag that has been used */
	protected static $_lastTag = '';

	/** @var string Last message that has been logged */
	protected static $_lastMessage = '';

	/** @var int Last log level that was outputted */
	protected static $_lastLogLevel = self::NONE;

	/** @var resource */
	protected static $_logFileHandle = null;

	/** @var string */
	protected static $_logFileName = 'ype.log';

	/**
	 * Sets the log level.
	 *
	 * @param  int       $logLevel
	 * @throws Exception If $logLevel is not a valid logLevel.
	 */
	public static function setLogLevel($logLevel)
	{
		if(is_numeric($logLevel))
		{
			if(!isset(self::$_logLevels[$logLevel]))
			{
				if($logLevel < 0)
				{
					$logLevel = 0;
				}
				elseif($logLevel >= max(array_keys(self::$_logLevels)))
				{
					$logLevel = max(array_keys(self::$_logLevels));
				}
			}
		}
		elseif(is_string($logLevel))
		{
			$logLevel       = strtoupper($logLevel);
			$logLevelValues = array_flip(self::$_logLevels);
			if(isset($logLevelValues[$logLevel]))
			{
				$logLevel = $logLevelValues[$logLevel];
			}
			else
			{
				throw new Exception("LogLevel '{$logLevel}'' is not a valid logLevel.");
			}
		}
		else
		{
			throw new Exception("LogLevel should be an integer or a string.");
		}

		self::$_logLevel = $logLevel;
	}

	public static function getLogLevel()
	{
		return self::$_logLevel;
	}

	public static function setLogFileName($logFileName)
	{
		self::$_logFileName   = $logFileName;
		self::$_logFileHandle = null;
	}

	/**
	 * Log an error.
	 *
	 * @param  string   $tag        Tag
	 * @param  string   $message    Log message
	 * @param  boolean  $appendable Whether this log entry should be appendable
	 */
	public static function error($tag, $message, $appendable = false)
	{
		if(self::ERROR <= self::$_logLevel)
		{
			self::_log(self::ERROR, $tag, $message, $appendable);
		}
	}

	/**
	 * Log a warning.
	 *
	 * @param  string   $tag        Tag
	 * @param  string   $message    Log message
	 * @param  boolean  $appendable Whether this log entry should be appendable
	 */
	public static function warning($tag, $message, $appendable = false)
	{
		if(self::WARNING <= self::$_logLevel)
		{
			self::_log(self::WARNING, $tag, $message, $appendable);
		}
	}

	/**
	 * Log an informational message.
	 *
	 * @param  string   $tag        Tag
	 * @param  string   $message    Log message
	 * @param  boolean  $appendable Whether this log entry should be appendable
	 */
	public static function info($tag, $message, $appendable = false)
	{
		if(self::INFO <= self::$_logLevel)
		{
			self::_log(self::INFO, $tag, $message, $appendable);
		}
	}

	/**
	 * Log a debugging message.
	 *
	 * @param  string   $tag        Tag
	 * @param  string   $message    Log message
	 * @param  boolean  $appendable Whether this log entry should be appendable
	 */
	public static function debug($tag, $message, $appendable = false)
	{
		if(self::DEBUG <= self::$_logLevel)
		{
			self::_log(self::DEBUG, $tag, $message, $appendable);
		}
	}

	public static function debugCallback($tag, $callback)
	{
		if(self::DEBUG <= self::$_logLevel)
		{
			// XXX debug stuff
			if(is_array($callback))
			{
				if(count($callback) > 1)
				{
					$object = $callback[0];
					$method = $callback[1];
					if(is_object($object))
					{
						$object = get_class($object);
					}

					$callback = "{$object}::{$method}";
				}
				elseif(1 == count($callback))
				{
					$callback = $callback[0];
				}
			}


			self::debug(__CLASS__, "callback: {$callback}().");
		}
	}

	/**
	 * Log information about the calling function to the debug log.
	 * @param mixed $result Additional data to log which should be the result of the function call.
	 */
	public static function debugFunctionCall($result = null)
	{
		if(self::DEBUG <= self::$_logLevel)
		{
			$backtrace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT);
			$frame     = $backtrace[1];

			$function = $frame['function'];
			$args     = $frame['args'];

			foreach($args as $key => $arg)
			{
				if(null === $arg)
				{
					$args[$key] = 'null';
				}
				elseif(false === $arg)
				{
					$args[$key] = 'false';
				}
				elseif(true === $arg)
				{
					$args[$key] = 'true';
				}
				elseif(is_object($arg))
				{
					$args[$key] = '[' . get_class($arg) . ' ' . json_encode($arg) . ']';
				}
				elseif(is_array($arg))
				{
					$args[$key] = json_encode($arg);
				}
				elseif(is_string($arg))
				{
					$args[$key] = "'{$arg}'";
				}
			}
			$tag = $frame['class'];
			if(strpos($tag, 'Ype_') === 0)
			{
				$tag = substr($tag, 4);
			}

			if($result !== null)
			{
				if(is_array($result) || is_object($result))
				{
					$result = ":\n" . print_r($result, true);
				}
				else
				{
					$result = ": {$result}";
				}
			}
			self::debug($tag, "{$function}(" . implode(', ', $args) . "){$result}");
		}
	}

	/**
	 * Log a verbose message.
	 *
	 * @param  string   $tag        Tag
	 * @param  string   $message    Log message
	 * @param  boolean  $appendable Whether this log entry should be appendable
	 */
	public static function verbose($tag, $message, $appendable = false)
	{
		if(self::VERBOSE <= self::$_logLevel)
		{
			self::_log(self::VERBOSE, $tag, $message, $appendable);
		}
	}

	/**
	 * Append to the current Log line.
	 *
	 * @param  string   $message    Log message
	 * @param  boolean  $appendable Whether this log entry should be appendable
	 */
	public static function append($message, $appendable = false)
	{
		if(self::$_lastLogLevel <= self::$_logLevel)
		{
			self::$_lastMessage .= $message;
			if(self::$_appendable)
			{
				self::_output($message, self::$_logLevelColors[self::$_lastLogLevel], true, $appendable);
			}
			else
			{
				self::_log(self::$_lastLogLevel, self::$_lastTag, self::$_lastMessage, $appendable);
			}
		}
	}

	/**
	 * Get a timestamp to use for logging.
	 *
	 * @param  string  $microtime  OPTIONAL microtimestamp in microtime(false) format
	 * @return string
	 */
	public static function getTimestamp($microtime = null)
	{
		if($microtime === null)
		{
			$microtime = microtime(false);
		}

		list($usec, $sec) = explode(' ', $microtime);
		$msec = floor($usec * 1000);
		$msec = str_pad($msec, 3, '0', STR_PAD_LEFT);

		return date('M  j H:i:s') . ".{$msec}";
	}

	/**
	 * Log a message.
	 *
	 * @param  int      $logLevel   Log level
	 * @param  string   $tag        Tag
	 * @param  string   $message    Log message
	 * @param  boolean  $appendable Whether this log entry should be appendable
	 */
	protected static function _log($logLevel, $tag = '', $message, $appendable)
	{
		if($logLevel <= self::$_logLevel)
		{
			self::$_lastLogLevel = $logLevel;

			if(strlen($tag) > 20)
			{
				$tag = '..' . substr($tag, -18);
			}

			if(is_object($message) || is_array($message))
			{
				$message = print_r($message, true);
			}

			self::$_lastTag     = $tag;
			self::$_lastMessage = $message;

			$tagLabel = str_pad(" [{$tag}]", 24, ' ', STR_PAD_RIGHT);

			self::_output(self::getTimestamp() . " " . self::$_logLevels[$logLevel] . $tagLabel, 'White', false, true);
			self::_output(" {$message}", '', true, $appendable);
		}
	}

	/**
	 * Output log stuff.
	 *
	 * @param  string   $string     Text to output
	 * @param  string   $color      Color to use
	 * @param  string   $appending  Whether we're currently appending to the same row
	 * @param  string   $appendable Whether this row should be appendable
	 */
	protected static function _output($string, $color = '', $appending, $appendable)
	{
		$output = '';
		if(!$appending && self::$_appendable)
		{
			$output .= PHP_EOL;
		}

		$output .= $string;

		if($appendable)
		{
			self::$_appendable = true;
		}
		else
		{
			self::$_appendable = false;
			$output .= PHP_EOL;
		}

		$handle = self::getLogFileHandle();
		fwrite($handle, $output);
	}

	/**
	 * Get handle for the logFile.
	 *
	 * @return resource
	 */
	public static function getLogFileHandle()
	{
		if(self::$_logFileHandle === null)
		{
			$logFileName          = Ype::ensureConfigPath(self::$_logFileName);
			self::$_logFileHandle = fopen($logFileName, 'a+');
		}

		return self::$_logFileHandle;
	}
}