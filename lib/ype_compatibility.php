<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2015 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @author    Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2016 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/** Make sure AF_UNIX is defined. */
if(!defined('AF_UNIX'))
{
	/** Local communication protocol family.
	 * High efficiency and low overhead make it a great form of IPC (Interprocess Communication). */
	define('AF_UNIX', 1);
}

/** Make sure AF_INET is defined. */
if(!defined('AF_INET'))
{
	/** IPv4 Internet based protocols.
	 * TCP and UDP are common protocols of this protocol family. */
	define('AF_INET', 2);
}

/** Make sure AF_INET6 is defined. */
if(!defined('AF_INET6'))
{
    define ('AF_INET6', 10);
}

/** Make sure SOCK_STREAM is defined. */
if(!defined('SOCK_STREAM'))
{
	define ('SOCK_STREAM', 1);
}

/** Make sure SOCK_DGRAM is defined. */
if(!defined('SOCK_DGRAM'))
{
	define ('SOCK_DGRAM', 2);
}

/** Make sure SOCK_RAW is defined. */
if(!defined('SOCK_RAW'))
{
	define ('SOCK_RAW', 3);
}

/** Make sure SOCK_RDM is defined. */
if(!defined('SOCK_RDM'))
{
	define ('SOCK_RDM', 4);
}

/** Make sure SOCK_SEQPACKET is defined. */
if(!defined('SOCK_SEQPACKET'))
{
	define ('SOCK_SEQPACKET', 5);
}

/** Make sure MSG_OOB is defined. */
if(!defined('MSG_OOB'))
{
	define ('MSG_OOB', 1);
}

/** Make sure MSG_PEEK is defined. */
if(!defined('MSG_PEEK'))
{
	define ('MSG_PEEK', 2);
}

/** Make sure MSG_DONTROUTE is defined. */
if(!defined('MSG_DONTROUTE'))
{
	define ('MSG_DONTROUTE', 4);
}

/** Make sure MSG_CTRUNC is defined. */
if(!defined('MSG_CTRUNC'))
{
	define ('MSG_CTRUNC', 8);
}

/** Make sure MSG_TRUNC is defined. */
if(!defined('MSG_TRUNC'))
{
	define ('MSG_TRUNC', 32);
}

/** Make sure MSG_WAITALL is defined. */
if(!defined('MSG_WAITALL'))
{
	define ('MSG_WAITALL', 256);
}

/** Check if lcfirst() exists. */
if(!function_exists('lcfirst'))
{
	/**
	 * Make a string's first character lowercase
	 * @link http://php.net/manual/en/function.lcfirst.php
	 * @param string $str The input string.
	 * @return string the resulting string.
	 */
	function lcfirst($str)
	{
		$str    = (string) $str;
		$str[0] = strtolower($str[0]);

		return $str;
	}
}
