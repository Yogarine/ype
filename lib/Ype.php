<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2015 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype
 * @author	  Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2015 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

// Check if lib dir is set.
if(!defined('YPE_LIB_DIR'))
{
	/** Fallback lib dir path. */
	define('YPE_LIB_DIR', realpath(dirname(__FILE__)));
}

/**
 * @package Ype
 */
class Ype
{
	/** Max value of a 32-bit unsigned integer. Used as alternative to 'infinite' in some code. */
	const MAX_INTEGER_UNSIGNED = 4294967295;
	/** Max value of a 32-bit signed integer. Used as alternative to 'infinite' in some code. */
	const MAX_INTEGER_SIGNED   = 2147483647;
	
	/** @var Ype_Broker */
	protected static $_broker = null;
	/** @var boolean Whether we're still supposed to run. */
	protected static $_isRunning = true;
	/** @var int The current line the cursor is currently positioned on. */
	protected static $_line = 1;
	/** @var int The column that the cursor is currently positioned on. */
	protected static $_column = 1;
	/** @var string[] */
	protected static $_history = array();
	/** @var string[] */
	protected static $_inputBuffer = array();
	/** @var boolean */
	protected static $_waitingForCPR = false;
	/** @var Ype_Session */
	protected static $_session = null;
	/** @var array */
	protected static $_settings = null;
	/** @var array */
	protected static $_defaultSettings = array(
		'ype' => array(
			'logLevel' => Ype_Log::WARNING,
		)
	);
	private static $_quitMessage = '';
	
	private static $_confDir = '';

	public static function setConfDir($confDir)
	{
		self::$_confDir = $confDir;
	}
	
	public static function getConfDir()
	{
		if(self::$_confDir == '')
		{
			self::$_confDir = realpath(getenv('HOME')) . '/.ype';
		}
		
		return self::$_confDir;
	}

	/**
	 * @return Ype_Broker
	 * @throws Exception  If no broker is set.
	 */
	public static function getBroker()
	{
		if(self::$_broker === null)
		{
			throw new Exception("No Broker set!");
		}
		
		return self::$_broker;
	}

	/**
	 * @param Ype_Broker $broker
	 */
	public static function setBroker(Ype_Broker $broker)
	{
		self::$_broker = $broker;
	}

	/**
	 * Super lightweight autoloader.
	 *
	 * @param string $className
	 */
	public static function autoload($className)
	{
		$classFilePath = YPE_LIB_DIR . DIRECTORY_SEPARATOR . str_replace(array('_', '\\'), DIRECTORY_SEPARATOR, $className) . '.php';
		if(file_exists($classFilePath))
		{
			/** @noinspection PhpIncludeInspection */
			require $classFilePath;
		}
		else
		{
			$namespace = null;
			$placeHolder = "<?php" . PHP_EOL;
			$lastBackslash = strrpos($className, '\\');
			if($lastBackslash !== false)
			{
				$namespace = trim(substr($className, 0, $lastBackslash), '\\');
				$className = substr($className, $lastBackslash + 1);
			}

			if($namespace)
			{
				$placeHolder .= "namespace {$namespace}" . PHP_EOL;
			}
			$placeHolder .= "class {$className} extends Ype_UnknownClass {" . PHP_EOL .
//			                "\tpublic static function __callStatic(\$name, \$arguments)" . PHP_EOL .
//			                "\t{" . PHP_EOL .
//			                "\t\tthrow new Ype_ErrorException(\"Call to undefined method {\$name}()\");" . PHP_EOL .
//							"\t}" . PHP_EOL .
			                "}" . PHP_EOL;

			$placeHolderFile = "ype_placeholder_{$className}." . getmypid() . '.php';

			Ype_Log::debug(__CLASS__, $placeHolder);
			self::writeToConfigFile($placeHolderFile, $placeHolder);

			require self::ensureConfigPath($placeHolderFile);
		}
	}

	public static function errorHandler($errno, $errstr, $errfile = null, $errline = null, array $errcontext = array())
	{
		Ype_Log::error('ype', "PHP error: ({$errno}) {$errstr} in {$errfile} on line {$errline}");
		ob_start();
		debug_print_backtrace();
		$backtrace = ob_get_contents();
		ob_end_clean();
		Ype_Log::debug('ype', $backtrace);

		throw new Ype_ErrorException($errstr, $errno, $errfile, $errline);
	}
	
	/**
	 * Get the config file path.
	 *
	 * @param  string  $configFilename  Filename in the config dir that you want
	 * @return string  The config filename path
	 */
	public static function ensureConfigPath($configFilename)
	{
		$path = self::getConfDir();

		if(!file_exists($path))
		{
			mkdir($path);
		}
		elseif(!is_dir($path))
		{
			// TODO proper error handling
			die("Config path '{$path}' already exists and is not a directory!\n");
		}

		return "$path/$configFilename";
	}

	/**
	 * @param  string $fileName
	 * @param  string $string
	 * @return void
	 */
	public static function writeToConfigFile($fileName, $string)
	{
		$fileName = self::ensureConfigPath($fileName);
		$handle   = fopen($fileName, 'w+');
		fwrite($handle, $string . PHP_EOL);
		fclose($handle);
	}

	/**
	 * @param  string $fileName
	 * @param  string $string
	 * @return void
	 */
	public static function appendToConfigFile($fileName, $string)
	{
		$fileName = self::ensureConfigPath($fileName);
		$handle   = fopen($fileName, 'a+');
		fwrite($handle, $string . PHP_EOL);
		fclose($handle);
	}

	/**
	 * @return void
	 */
	public static function printUsage()
	{
		echo "Usage: ype\n";
	}
	
	/**
	 */
	public static function run()
	{
		$broker = self::getBroker();

		Ype_Log::debug('Ype', "Going into waitForStreams loop. (_isRunning: '" . self::$_isRunning . "')");

		do
		{
			$hasStreams = false;
			try
			{
				$hasStreams = $broker->wait(60);
			}
			catch(Exception $e)
			{
				Ype_Log::error('Ype', "{$e->getMessage()}");
				self::message('quit', array("{$e->getMessage()}"));
				self::quit("Uncaught Exception: {$e->getMessage()} in {$e->getFile()}:{$e->getLine()}");
			}
		}
		while(self::$_isRunning || $hasStreams);

		$broker->closeAll();

		Ype_Log::info(__CLASS__, "Quit: '" . self::$_quitMessage . "'");

		return self::$_quitMessage;
	}

	public static function quit($message = '')
	{
		Ype_Log::info('quit', $message);

		Ype::getBroker()->quit();
		self::$_quitMessage = $message;
		self::$_isRunning   = false;
	}

	protected static function _getSettings()
	{
		if(self::$_settings === null)
		{
			$fileName = self::ensureConfigPath('settings');
			if(file_exists($fileName))
			{
				self::$_settings = parse_ini_file($fileName, true);
				self::$_history = file($fileName);
			}
			else
			{
				self::$_settings = self::$_defaultSettings;
			}
		}

		return self::$_settings;
	}

	/**
	 * @param          $key
	 * @param  string  $section
	 * @param  mixed   $default  OPTIONAL
	 * @return string|int
	 */
	public static function getSetting($key, $section = 'ype', $default = null)
	{
		$settings = self::_getSettings();

		if(array_key_exists($section, $settings))
		{
			if(array_key_exists($key, $settings[$section]))
			{
				return $settings[$section][$key];
			}
		}

		return $default;
	}

	/**
	 * @param string     $messageName
	 * @param array      $params
	 * @param int|string $target
	 * @param int        $messageToken
	 * @param int        $senderId
	 * @return int
	 */
	public static function message($messageName, array $params = array(), $target = null, $messageToken = null, $senderId = null)
	{
		return self::getBroker()->sendMessage($messageName, $params, $target, $messageToken, $senderId);
	}

	public static function getCurrentMessageToken()
	{
		return self::getBroker()->getCurrentMessageToken();
	}

	public static function getCurrentSenderId()
	{
		return self::getBroker()->getSenderPluginId();
	}
}